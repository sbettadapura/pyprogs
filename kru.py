from collections import defaultdict
if __name__ == '__main__':
	import sys
	f = open(sys.argv[1])
	g_nodes, g_edges = map(int, f.readline().split())
	gd = sorted([(w, u, v) for u, v, w in [map(int, f.readline().split()) for i in range(g_edges)]])

	nei = defaultdict(set)

	tree_weight = 0
	for w, u, v in gd:
		if u in nei[v] or v in nei[u]:
			continue

		s = nei[u] |  nei[v] | {u, v}
		for x in s:
			nei[x] = s
		tree_weight += w
	print tree_weight
