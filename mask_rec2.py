#!/usr/bin/env python
"""
	Given a width, produce all bit combinations.
	Given 3, 000, 001, 010, 011, 100, 101, 110, 111.
"""
def mask(n):
	if n == 1:
		return ['0', '1']
	l = mask(n-1)
	x = []
	for i in l:
		x.append(i + '0')
		x.append(i + '1')
	return x

if __name__ == "__main__":
	import sys
	try:
		print mask(int(sys.argv[1]))
	except:
		print "usage %s [width of mask]" % sys.argv[0]
		exit(1)
