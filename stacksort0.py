#!/usr/bin/env python

def stacksort(src):
	dest = []
	minbot = -100
	while src:
		bot = src.pop()
		while src:
			x = src.pop()
			if x < bot:
				dest.append(bot)
				bot = x
			else:
				dest.append(x)
		while dest and dest[-1] > minbot:
			src.append(dest.pop())
		minbot = bot
		dest.append(bot)	
	return dest

if __name__ == "__main__":
	from random import randrange
	print stacksort([randrange(1000) for i in range(randrange(25))])

