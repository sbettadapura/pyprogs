#!/usr/bin/env python

from collections import defaultdict
from operator import add

def print_magic(a, cost):
	print cost
	for i in range(3):
		print tuple(a[i])

def magic(a, cost = 0):
	idx = defaultdict(int)
	for i in range(3):
		for j in range(3):
			idx[a[i][j]] += 1
	missing = any([idx[i] == 0 for i in range(1, 10)])
	rowdelta = [15 - sum(a[i]) for i in range(3)]
	coldelta = [15 - sum(a[i][j] for i in range(3)) for j in range(3)]
	candidates = reduce(add, [[(i, j, rowdelta[i]) for j in range(3) if rowdelta[i] != 0] for i in range(3)])

	if candidates == []:
		candidates = reduce(add, [[(i, j, coldelta[j]) for i in range(3) if coldelta[j] != 0] for j in range(3)])
		
	if candidates == [] and not missing and all(coldelta[i] == 15 for i in range(3)):
		print_magic(a, cost)
		return cost

	mincost = float('inf')
	for z in candidates:
		x, y, delta = z
		tmp = a[x][y]
		a[x][y] += delta	
		if a[x][y] > 9:
			a[x][y] = tmp
			continue
		mycost = magic(a, cost + abs(delta))
		if mycost < mincost:
			mincost = mycost
		a[x][y] = tmp

	return mincost	
if __name__ == "__main__":
	square = [[6, 2, 7], [8, 3, 4], [1, 5, 9]]
	print magic(square)
