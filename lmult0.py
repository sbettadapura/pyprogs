#!/usr/bin/env python

def lmult(a, b):
	an = len(a)
	bn = len(b)
	accum = []
	for i in range(bn - 1, -1, -1):
		ax = [b[i] * x for x in a]
		zerosb = [0 for j in range(bn -i - 1)]
		zerosf = [0 for j in range(i)]
		ax += zerosb
		zerosf += ax
		accum.append(zerosf)
	carry, res = 0, []
	while True:
		try:
			y = [x.pop() for x in accum]
		except:
			break	
		ps = carry + sum(y)	
		dig = ps % 10
		carry = ps / 10
		res.append(dig)
	if carry: res.append(carry)
	res.reverse()
	print "a = ", a, " b = ", b, " a * b = ", res	
if __name__ == "__main__":
	import sys
	lmult(list(map(int, sys.argv[1])), list(map(int, sys.argv[2])))
