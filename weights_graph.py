#!/usr/bin/env python
"""
	A riddle from 1612 in France. What's the minimum number of weights that
  	you can use on either side of a balance (but only one of each kind) and 
	produce weights from 1 to 40 ? The answer is 1, 3, 9, 27. 2 = 3-1, 4 = 3+1,
	5 = 9 - 3 - 1 etc. It also works for higher powers of 3 like 
	1, 3, 9, 27, 81, ...

	The simpler problem is: given weights 3 ** 0, 3 ** 1 ... 3 ** n, can you 
 	figure out all the weights you can derive and describe the derivation 
	for each weight ?
"""
from collections import defaultdict
import sys
try:
	num = int(sys.argv[1])
except:
	print "usage %s [highest power of 3 to use]" % sys.argv[0]
	exit(1)

weights = [3 ** x for x in range(num+1)]
g = {0: set()}
Q = {0}

npops = 0
while Q:
	n = Q.pop()
	npops += 1
	for w in weights:
		for i in (1, -1):
			new = n + w * i
			if new > 0 and new not in g and w not in g[n] and -w not in g[n]:
				g[new] = g[n] | {w * i}
				Q.add(new)

for k in g.keys()[1:]:
	print k, ": ", sorted(list(g[k]), reverse=True)
print "npops = ", npops
