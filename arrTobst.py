#!/usr/bin/env python

class Tree:
	def __init__(self, root, left = None, right = None):
		self.root = root
		self.left = left
		self.right = right

def atob(arr):
	n = len(arr)
	if n == 0:
		return None
	elif n == 1:
		return(Tree(arr[0]))

	pos = n / 2
	return Tree(arr[pos], atob(arr[:pos]), atob(arr[pos+1:]))
if __name__ == "__main__":
	def print_tree(tree):
		if tree.left is not None:
			print_tree(tree.left)
		print tree.root
		if tree.right is not None:
			print_tree(tree.right)
		
	import sys
	from random import randrange
	try:
		num = int(sys.argv[1])
	except:
		print "usage %s [num of elements in tree]" % sys.argv[0]
		exit(1)	
	l = sorted([randrange(20 * num) for i in range(num)])
	print l
	tree = atob(l)
	print_tree(tree)
