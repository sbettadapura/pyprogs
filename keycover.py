#!/usr/bin/env python

text = "ab def cd ght kjhj chhh bc kj asf bc hjhh cd kljk ab hjhj bc kk cd"
keywords = ["ab", "bc", "cd"]
from collections import defaultdict
keytopos = defaultdict(list)
covered = defaultdict(int)
for kw in keywords:
	i = 0
	while i < len(text):
		if kw in text[i:]:
			x = text[i:].index(kw)
			keytopos[kw].append(x + i)
			i += x + 2
		else:
			break
cover_seq = sorted([keytopos[kw][0] for kw in keywords])
from operator import add
allkeypos = sorted(reduce(add, [keytopos[kw] for kw in keywords]))
min_cover_seq = allkeypos.index(cover_seq[0])
max_cover_seq = allkeypos.index(cover_seq[-1])
mins, maxs = min_cover_seq, max_cover_seq
print cover_seq, min_cover_seq, max_cover_seq	
print allkeypos
while max_cover_seq < len(allkeypos):
	for x in range(min_cover_seq, max_cover_seq):
		for kw in keywords:
			if x in keytopos[kw]:
				covered[kw] += 1	

	for kw in keywords:
		if covered[kw] == 0:
			max_cover_seq += 1
			break
	else:
		mins, maxs = min_cover_seq, max_cover_seq
print mins, maxs

