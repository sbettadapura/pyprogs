#!/usr/bin/env python

def minp(a, b, k):
	print a, b, k
	xa = a[:k]
	n = len(a)
	while xa and b:
		if xa[-1] > b[-1]:
			outval = xa.pop()
		else:
			outval = b.pop()
		n -= 1
		a[n] = outval
	if xa: src = xa
	else: src = b
	while src:
		n -= 1
		a[n] = src.pop()

	print a	

if __name__ == "__main__":
	import sys
	from random import randrange
	try:
		an, bn = map(int, sys.argv[1:])
	except:
		print "usage %s [size1 size2]" % sys.argv[0]
		exit(1)
	a = sorted([randrange(an * an) for x in range(an)])
	b = sorted([randrange(an * an) for x in range(bn)])
	a += [0 for x in b]
	#a = [9, 13, 16, 19, 19, 0, 0]
	#b = [4, 5]
	minp(a, b, an)
