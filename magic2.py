#!/usr/bin/env python

from collections import defaultdict
from operator import add

def print_magic(a, cost):
	print cost
	for i in range(3):
		print tuple(a[i])

def swap_nums(a, cand, curr, i, j, idx):
	if cand > 0 and cand < 10 and idx[cand] == 0:
		idx[curr] -= 1
		a[i][j] = cand
		idx[cand] = 1
		cost = abs(curr - cand)
		return cost
	else:
		return 0

def replace_num(a, i, j, idx):
	curr = a[i][j]
	cand = 15 - (sum(a[i]) - curr)
	return swap_nums(a, cand, curr, i, j, idx)

def replace_num_col(a, i, j, idx):
	curr = a[i][j]
	cand = 15 - (sum(a[i][j] for i in range(3))) + curr
	return swap_nums(a, cand, curr, i, j, idx)

def deduplicate(a, idx):
	cost = 0
	for i in range(3):
		for j in range(3):
			if idx[a[i][j]] > 1:
				cost +=  replace_num(a, i, j, idx)
	return cost

def miss(a):
	idx = defaultdict(int)
	for i in range(3):
		for j in range(3):
			idx[a[i][j]] += 1
	missing = any([idx[i] == 0 for i in range(1, 10)])
	return missing, idx
	
def row_adjust(a, idx):
	rowdelta = [15 - sum(a[i]) for i in range(3)]
	cost = 0
	for i in range(3):	
		if rowdelta[i] != 0:
			for j in range(3):
				cost += replace_num(a, i, j, idx)
	return cost

def col_adjust(a, idx):
	coldelta = [15 - sum(a[i][j] for i in range(3)) for j in range(3)]
	cost = 0
	for j in range(3):	
		if coldelta[j] != 0:
			for i in range(3):
				cost += replace_num_col(a, i, j, idx)
	return cost

def check_magic(a):
	rowdelta = [15 - sum(a[i]) for i in range(3)]
	coldelta = [15 - sum(a[i][j] for i in range(3)) for j in range(3)]
	return sum(rowdelta) + sum(coldelta) == 0

def swap_two_entries(a, p, q):
	xp, yp = p
	xq, yq = q
	tmpp, tmpq = a[xp][yp], a[xq][yq]
	a[xp][yp], a[xq][yq] = a[xq][yq], a[xp][yp]
	if check_magic(a):
		return abs(a[xp][yp] - tmpp) + abs(a[xq][yq] - tmpq)
	else:
		a[xp][yp], a[xq][yq] = tmpp, tmpq
		return 0
	
def col_intra_adjust(a, idx):
	cost = 0
	for row in range(3):
		rsum = sum(a[row])
		if rsum == 15:
			continue
		for col in range(3):
			if row == 0:
				mycost = swap_two_entries(a, (0, col), (1, col))
				if mycost != 0:
					return cost + mycost
				else:
					mycost = swap_two_entries(a, (0, col), (2, col))
					if mycost != 0:
						return cost + mycost
			elif row == 1:
				mycost = swap_two_entries(a, (1, col), (2, col))
				if mycost != 0:
					return cost + mycost
	return 0

def row_intra_adjust(a, idx):
	cost = 0
	for col in range(3):
		csum = sum([a[i][col] for i in range(3)])
		if csum == 15:
			continue
		for row in range(3):
			if col == 0:
				mycost = swap_two_entries(a, (row, 0), (row, 1))
				if mycost != 0:
					return cost + mycost
				else:
					mycost = swap_two_entries(a, (row, 0), (row, 2))
					if mycost != 0:
						return cost + mycost
			elif col == 1:
				mycost = swap_two_entries(a, (row, 1), (row, 2))
				if mycost != 0:
					return cost + mycost
	return 0

def magic(a, cost = 0):
	missing, idx = miss(a)
	cost = 0
	while missing:
		newcost = deduplicate(a, idx)
		if newcost == 0:
			break
		cost += newcost
		missing, idx = miss(a)

	while missing:
		newcost = row_adjust(a, idx)
		if newcost == 0:
			break
		cost += newcost
		missing, idx = miss(a)

		newcost = col_adjust(a, idx)
		if newcost == 0:
			break
		cost += newcost
		missing, idx = miss(a)

	rows_ok = all([15 - sum(a[i])  == 0 for i in range(3)])
	cols_ok = all([15 - sum(a[i][j] for i in range(3))  == 0 for j in range(3)])
	if cols_ok and rows_ok:
		return cost, a
	elif rows_ok:
		cost += row_intra_adjust(a, idx)
	elif cols_ok:
		cost += col_intra_adjust(a, idx)
	return cost, a

if __name__ == "__main__":
	square = [[2, 2, 7], [8, 6, 4], [1, 2, 9]]
	print magic(square)
	square = [[4, 8, 2], [4, 5, 7], [6, 1, 6]]
	print magic(square)
	square = [[2, 7, 6], [8, 5, 1], [4, 4, 6]]
	print magic(square)
	square = [[7, 4, 9], [2, 6, 2], [2, 8, 1]]
	print magic(square)
