#!/usr/bin/env python

def nonrecur(a):
	potential, visited = [], set()
	for x in a:
		if x in visited:
			if x in potential:
				potential.remove(x)
		else:
			potential.append(x)
			visited.add(x)
	if potential:
		return potential[0]
	else:
		return None

if __name__ == "__main__":
	from random import randrange
	a = [randrange(15) for i in range(20)]
	print a
	print sorted(a)
	print nonrecur(a)
