#!/usr/bin/env python
"""
	A careless programmer dropped all the periods in an IP addr, so all you
	have is a string consisting of the original digits. Find all possible
	valid IP address in the given string.
"""

def ipaddr(addr_str, num_dots = 3, prefix = ""):
	if addr_str == "":
		return

	if num_dots == 0:
		if int(addr_str) < 256:
			print prefix + addr_str
		return

	for i in range(1, len(addr_str)):
		substr = addr_str[:i]	
		if int(substr) < 256:
			ipaddr(addr_str[i:], num_dots - 1, prefix + substr + ".")

if __name__ == "__main__":
	import sys
	try:
		x = int(sys.argv[1])
	except:
		print "usage: %s <valid IP addr without periods>" % sys.argv[0]
		exit(1)
	ipaddr(sys.argv[1])
