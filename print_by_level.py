#!/usr/bin/env python
class Tree:
	def __init__(self, val, left = None, right = None):
		self.val = val
		self.left = left
		self.right = right

from collections import deque
def print_by_level0(tree):
	next_level = []
	curr_level = deque([tree])
	lev = 0
	while curr_level:
		curr_level = deque(curr_level)
		print (4 - lev * 2) * " ",
		while curr_level:
			x = curr_level.popleft()
			print x.val,
			if x.left:
				next_level.append(x.left)
			if x.right:
				next_level.append(x.right)
		print "\n"
		curr_level = deque(next_level)
		next_level = []
		lev += 1
def print_by_level(tree):
	next_level = []
	curr_level = [tree]
	lev = 0
	while curr_level:
		print (4 - lev * 2) * " ",
		while curr_level:
			x = curr_level.pop()
			print x.val,
			if x.left:
				next_level.append(x.left)
			if x.right:
				next_level.append(x.right)
		print "\n"
		next_level.reverse()
		curr_level = next_level
		next_level = []
		lev += 1
if __name__ == "__main__":
	tl = Tree(50, Tree(25), Tree(75))
	tr = Tree(150, Tree(125), Tree(175))
	t = Tree(100, tl, tr)
	print_by_level0(t)
