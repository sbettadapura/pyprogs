#!/usr/bin/env python

def check_magic(a):
	rows_ok = all([15 - sum(a[i])  == 0 for i in range(3)])
	cols_ok = all([15 - sum(a[i][j] for i in range(3))  == 0 for j in range(3)])
	return rows_ok and cols_ok

def neighbors(node):
	res = []
	x, y = node
	wheel = [(0, 1), (0, -1), (1, 0), (-1, 0)]
	for wx, wy in wheel:
		xx = x + wx
		yy = y + wy
		if xx >= 0 and xx < 3 and yy >= 0 and yy < 3:
			res.append((xx, yy))	
	return res

def addbor(orig, skel, nodes, vals, cost, i, j, val):
	skel[i][j] = val
	thiscost = magic(orig, 	skel, [(i, j)] + nodes, [val] + vals, cost + abs(val - orig[i][j]))
	return thiscost

def magic(orig, skel, nodes, vals, cost):
	if len(nodes) == 9:
		if check_magic(skel):
			return cost
		else:
			return float('inf')

	mincost = float('inf')
	thiscost = float('inf')
	for node in nodes:
		for i, j in neighbors(node):
			if skel[i][j] != 0:
				continue

			x, y = node
			row_ext = i == x

			if row_ext:
				num_zeros = sum(int(skel[i][q] == 0) for q in range(3))
				xsum = sum(skel[i])
			else:
				num_zeros = sum(int(skel[q][j] == 0) for q in range(3))
				xsum = sum(skel[p][j] for p in range(3))
			if num_zeros == 0:
				continue

			tmp = skel[x][y]
			if num_zeros == 1:
		 		poten = 15 - xsum
				maybe = poten > 0 and poten < 10 and poten not in vals
			else:
				poten = 15 - xsum - orig[i][j]
				maybe = poten > 0 and poten < 10 and poten not in vals

			if orig[i][j] not in vals and maybe:
				thiscost = addbor(orig, skel, nodes, vals, cost, i, j, orig[i][j])
			else:
				if num_zeros == 1:
					poten = 15 - xsum
					maybe = poten > 0 and poten < 10 and poten not in vals
					if maybe:
						thiscost = addbor(orig, skel, nodes, vals, cost, i, j, poten)
				else:
					for num in range(1, 10):
						poten = 15 - skel[x][y] - num
						maybe = poten > 0 and poten < 10 and num not in vals
						if maybe:
							thiscost = addbor(orig, skel, nodes, vals, cost, i, j, num)
			skel[x][y] = tmp
			if thiscost < mincost:
				mincost = thiscost
	return mincost

if __name__ == "__main__":
	#square = [[randrange(1, 10) for j in range(3)] for i in range(3)]
	#square = [[3, 6, 2], [5, 5, 6], [7, 2, 8]]
	#square = [[3, 9, 1], [5, 4, 6], [7, 2, 8]]
	#square = [[3, 9, 1], [5, 4, 6], [7, 2, 8]]
	#square = [[3, 6, 1], [4, 5, 6], [7, 2, 8]]
	#square = [[3, 6, 2], [5, 5, 6], [7, 2, 8]]
	mincost = float('inf')
	#square = [[2, 2, 7], [8, 6, 4], [1, 2, 9]]
	square = [[4, 8, 2], [4, 5, 7], [6, 1, 6]]
	for i in range(3):
		for j in range(3):
			skel = [[0 for q in range(3)] for p in range(3)]
			skel[i][j] = square[i][j]	
			cost = magic(square, skel, [(i, j)], [skel[i][j]], 0)
			if cost < mincost:
				mincost = cost
	print cost
