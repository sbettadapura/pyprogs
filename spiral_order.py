#!/usr/bin/env/python

def spiral(n):
	sporder = []
	for m in range((n  + 1)/ 2):
		size = n - 2 * m
		start = 1 + m * (n + 1)
		if size == 1:
			sporder += [start]
			continue
		sporder += [start + x for x in range(size - 1)]
		sporder += [start + size - 1 + x * n for x in range(size - 1)]
		sporder += [start + size - 1 + (size - 1) * n - x for x in range((size - 1))]
		sporder += [start + (size - 1) * n - n * x for x in range((size - 1))]
	print "size of spiral order = ", len(sporder)
	return sporder
	
if __name__ == "__main__":
	import sys
	try:
		num = int(sys.argv[1])
	except:
		print "usage %s [size of square]" % sys.argv[0]
		exit(1)
	print spiral(num)
