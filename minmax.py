#!/usr/bin/env python

def minmax(a):
	mina = maxa = a[0]
	for x in a[1:]:
		if x < mina:
			mina = x
		if x > maxa:
			maxa = x
	return (mina, maxa)

if __name__ == "__main__":
	import sys
	from random import randrange
	n = int(sys.argv[1])
	a = [randrange(n *n ) for i in range(n)]
	print a
	x, y = minmax(a)
	print x, y
	print sorted(a)
