#!/usr/bin/env python

def long_equals(a):
	curr, maxl = a[:1], 1
	for x in a[1:]:
		if curr[-1] == x:
			curr.append(x)
		else:
			maxl = max(maxl, len(curr))
			curr = [x]
	return max(maxl, len(curr))

if __name__ == "__main__":
	print long_equals([2, 1, 3, 3, 5, 5, 5, 7, 7, 7, 7])
			

