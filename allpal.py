#!/usr/bin/env python
"""
	Given a string like 'abbccd' find all palindromes
"""
def perm(str, cnt, prefix=""):
	if cnt == 0:
		return [prefix]
	return reduce(lambda x, y: x + y, [perm(str[:i] + str[i+1:], cnt - 1, prefix + str[i]) for i in range(len(str))])

import sys
try:
	rest = sys.argv[1]
except:
	print "usage %s [palindrome source]" % sys.argv[0]
	exit(1)

redu = ""
for c in rest:
	if rest.count(c) > 1:
		rest = rest.replace(c, '', 2)
		redu += c	

for i in  range(1, len(redu) + 1):
	for p in perm(redu, i):
		for r in rest:
			print p + r + ''.join(reversed(list(p)))
