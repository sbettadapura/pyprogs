#!/usr/bin/env python
class Tree:
	def __init__(self, val, left = None, right = None):
		self.root = val
		self.left = left
		self.right = right
def covers(root, node, path):
	if root is None:
		return False
	if root.root == node.root:
		return True
	if root.left:
		path += [root.left]
		if covers(root.left, node, path):
			return True
		else:
			path.pop()

	if root.right:
		path += [root.right]
		if covers(root.right, node, path):
			return True
		else:
			path.pop()
	return False

if __name__ == "__main__":
	t1 = Tree(5)
	t2 = Tree(20)
	t3 = Tree(10, t1, t2)
	t4 = Tree(40)
	t5 = Tree(60)
	t6 = Tree(50, t4, t5)
	t7 = Tree(30, t3, t6)
	path1, path2 = [t7], [t7]
	if covers(t7, t2, path1) and covers(t7, t4, path2):	
		anc = path1[0]
		l = min(len(path1), len(path2))
		for j in range(1, l):
			if path1[j] != path2[j]:
				break
			else:
				anc = path1[j] 
		print "lca = ", anc.root
