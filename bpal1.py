#!/bin/python

import os
import sys

#
# Complete the buildPalindrome function below.
#
def checkpal(asub, b):
	asubl = list(asub)
	arev = ''.join([y for y in reversed(asubl)])
	res = ''
	if arev in b:
		ind =  b.index(arev)
		if ind > 0:
			res = asub + b[ind - 1] + arev
		else:
			res = asub + arev
	elif len(arev) > 1 and arev[1:] in b:
		res = asub + arev[1:]

	return res

def buildPalindrome(a, b):
    #
	max_res = ''
	for i in xrange(1, len(a) + 1):
		for j in xrange(len(a)):
			if j + i > len(a):
				continue
			res = checkpal(a[j:j+i], b)
			if len(res) > len(max_res):
				max_res = res
	if max_res == '':
		max_res = str(-1)
	return max_res
if __name__ == '__main__':
	f = open(sys.argv[1])
	#fptr = open(os.environ['OUTPUT_PATH'], 'w')
	#t = int(raw_input())
	t = int(f.readline().rstrip())
	for t_itr in xrange(t):
		#a = raw_input()
		a = f.readline().rstrip()
		#b = raw_input()
		b = f.readline().rstrip()
		#fptr.write(result + '\n')
		result = buildPalindrome(a, b)
		print result
	#fptr.close()

