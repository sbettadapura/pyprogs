#!/usr/bin/env python

def buysell_1(a):
	print a
	low, max_diff, pair = a[0], 0, None
	for i in range(1, len(a)):
		if a[i] > low:
			diff = a[i] - low
			if diff > max_diff:
				max_diff = diff
				pair = (a[i], low)
		elif a[i] < low:
			low = a[i]
	print max_diff, pair

def buysell_2(a):
	print a
	low, diff = a[0], 0
	for p in a[1:]:
		diff = max(diff, p - low)
		low = min(low, p)
	print diff
if __name__ == "__main__":
	x = [310, 315, 275, 295, 260, 270, 290, 230, 255, 250]
	buysell_1(x)
	buysell_2(x)
