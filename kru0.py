if __name__ == '__main__':
	g_nodes, g_edges = map(int, raw_input().split())
	g_from = [0] * g_edges
	g_to = [0] * g_edges
	g_weight = [0] * g_edges

	def neighbors(u, es):
		Q, nei = [u], set()
		while Q:
			z = Q.pop()	
			if z in nei:
				continue
			nei.add(z)
			for edge in es:
				x, y = edge
				if z == x:
					Q.append(y)
				elif z == y:
					Q.append(x)
		return nei
	
	for i in xrange(g_edges):
		g_from[i], g_to[i], g_weight[i] = map(int, raw_input().split())

	gd, ns, es= dict(), set(), set()
	for i in range(len(g_weight)):
		gd[g_weight[i]] = (g_from[i], g_to[i])
	tree_weight = 0
	while not (len(ns) == g_nodes and len(es) == (g_nodes - 1)):
		for k in sorted(gd.keys()):
			u, v = gd[k]
			if neighbors(u, es) & neighbors(v, es) == set():
				ns.add(u)
				ns.add(v)
				es.add((u, v))
				tree_weight += k
	print ns, es
	print g_nodes, len(es)
	print tree_weight
