#!/usr/bin/env python

def change_ways(amt, denom):
	ways = 0
	for i in range(len(denom)):
		x = denom[i]
		if amt < x:
			continue
		elif amt == x:
			ways += 1
		else:
			ways += change_ways(amt - x, denom[i:])
	return ways

if __name__ == "__main__":
	import sys
	amt = int(sys.argv[1])
	print change_ways(amt, [25, 10, 5, 1])	
