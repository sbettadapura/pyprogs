#!/usr/bin/env python
def allowed_rows(col, queens):
	forbidden = set([q[0] for q in queens])
	forbidden |= set([q[0] + (q[1] - col) * i for q in queens for i in (1, -1)])
	return set(range(8)) - forbidden	
	
def eightq(res, queens = [], st_col = 0):
	if len(queens) == 8:
		res.append(queens[:])
		return

	for col in range(st_col, 8):
		for row in allowed_rows(col, queens):
			queens.append((row, col))
			eightq(res, queens, col + 1)
			queens.pop()

if __name__ == "__main__":
	res = []
	eightq(res)
	for x in res:
		print x
