#!/usr/bin/env python

def well(exp):
	pst = []
	for par in exp:
		if par in "({[":
			pst.append(par)
		else:
			x = pst.pop()
			if x is None:
				print "Not well formed"
				break
			elif x != opposite[par]:
				print "Not well formed"
				break
	else:
		if pst:
			print "Not Well formed"
		else:
			print "Well formed"
if __name__ == "__main__":
	import sys
	opposite = dict(zip(")}]" ,"({["))
	well(sys.argv[1])
