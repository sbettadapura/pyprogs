#!/usr/bin/env python
"""
	Given an array of numbers 'heapify' them in-place.
"""

def myheapify(myheap):
	pos = len(myheap) - 1
	while pos > 0:
		parent = (pos - 1) / 2
		if myheap[parent] < myheap[pos]:
			break
		else:
			myheap[parent], myheap[pos] = myheap[pos], myheap[parent]
			pos = parent

if __name__ == "__main__":
	import sys
	from random import randrange
	from heapq import heapify, heappop
	try:
		num = int(sys.argv[1])
	except:
		print "usage %s [size of array to heapify]" % sys.argv[0]
		exit(1)
	source = [randrange(num * 20) for i in range(num)]
	myheap = []
	for i in range(num):
		myheap.append(source[i])
		myheapify(myheap)
	print "source  = ", source
	print "myheap after myheapify() = ", myheap
	for i in range(num):
		for j in (2 * i + 1, 2 * i + 2):
			if j <= num - 1:
				assert(myheap[i] <= myheap[j])
	heapify(source)
	print "myheap after heapify() = ", source
	print [heappop(myheap) for x in range(len(myheap))]
	print [heappop(source) for x in range(len(source))]
