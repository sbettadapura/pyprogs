#!/usr/bin/env python
"""
	Given a width, produce all bit combinations.
	Given 3, 000, 001, 010, 011, 100, 101, 110, 111.
"""

def mask(cnt):
	for i in range(2, cnt+1):
		x = []
		for j in masks[i-1]:
			x.append(j + '0')
			x.append(j + '1')
		masks[i] = x

masks = {1: ['0', '1']}
if __name__ == "__main__":
	import sys
	try:
		mask(int(sys.argv[1]))
		print masks
	except:
		print "usage %s [width of mask]" % sys.argv[0]
		exit(1)
