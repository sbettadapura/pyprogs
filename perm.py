#!/usr/bin/env python
"""
	Print permutations of a n-character string taken r at a time.
	nPr.
"""
def perm(str, cnt, prefix=""):
	if cnt == 0:
		return [prefix]
	return reduce(lambda x, y: x + y, [perm(str[:i] + str[i+1:], cnt - 1, prefix + str[i]) for i in range(len(str))])
if __name__ == "__main__":
	import sys
	try:
		print perm(sys.argv[1], int(sys.argv[2]))
	except:
		print "usage %s [string] [no. of chars in permutation]" % sys.argv[0]
		exit(1)
