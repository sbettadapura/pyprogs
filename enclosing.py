#!/usr/bin/env python

def enclose(a, enc, left_right):
	left, right = 0, len(a) - 1
	res = -1
	while left <= right:
		mid = (left + right) / 2
		if a[mid] > enc:
			right = mid - 1
		elif a[mid] < enc:
			left = mid + 1
		else:
			res = mid
			if left_right == 0:
				right = mid - 1
			else:
				left = mid + 1
	return res

if __name__ == "__main__":
	a = [1,2,2,4,4,4,7,11,11,13]
	print enclose(a, 6, 0), enclose(a, 6, 1)
