#!/usr/bin/env python
from operator import mul

# Complete the journeyToMoon function below.
def journeyToMoon(n, astronaut):
	NUM_ASTROS_TO_PICK = 2
	sets, cnts = [], []
	for id0, id1 in astronaut:
		for s in sets:
			if id0 in s:
				sx = s
				break
		else:
			sx = {id0}
			sets.append(sx)	
		for t in sets:
			if id1 in t:
				if sx != t:
					sx |= t
					sets.remove(t)
				break
		else:
			sx.add(id1)
	for i in range(n):
		if not any(i in s for s in sets):
			sets.append({i})	
	for s in sets:
		cnts.append(len(s))
	print "sets = ", sets, "cnts = ", cnts
        
	def comb(cl, num, partial = [], res = []):
		if num == 0:
			res.append(reduce(mul, [p for p in partial]))
			return
		for i in range(len(cl)):
			comb(cl[i+1:], num - 1, partial + [cl[i]], res)
		return sum(res)
	return comb(cnts, NUM_ASTROS_TO_PICK)
if __name__ == "__main__":
	res = journeyToMoon(10000, [(1, 2), (3, 4)])
	#res = journeyToMoon(5, [(0, 1), (2, 3), (0, 4)])
	#res = journeyToMoon(10, [(0, 2), (1, 8), (1, 4), (2, 8), (2, 6), (3, 5), (6, 9)])
	#res = journeyToMoon(100, [(0, 11), (2, 4), (2, 95), (3,48), (4, 85), (4, 95), (5, 67), (5, 83), (5, 42), (6, 76), (9, 31), (9, 22), (9, 55), (10, 61), (10, 38), (11, 96), (11, 41), (12, 60), (12, 69), (14, 80), (14, 99), (14, 46), (15, 42), (15, 75), (16, 87), (16, 71), (18, 99), (18, 44), (19, 26), (19, 59), (19, 60), (20, 89), (21, 69), (22, 96), (22, 60), (23, 88), (24, 73), (27, 29), (30, 32), (31, 62), (32, 71), (33, 43), (33, 47), (35, 51), (35, 75), (37, 89), (37, 95), (38, 83), (39, 53), (41, 84), (42, 76), (44, 85), (45, 47), (46, 65), (47, 49), (47, 94), (50, 55), (51, 99), (53, 99), (56, 78), (66, 99), (71, 78), (73, 98), (76, 88), (78, 97), (80, 90), (83, 95), (85, 92), (88, 99), (88, 94)])
	print res
