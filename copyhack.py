#!/bin/python

import math
import os
import random
import re
import sys
from collections import defaultdict
from bisect import bisect, bisect_left
def gscore_word(gdict, chr, first, last):
    inds, vals = gdict[chr]
    x = bisect_left(inds, first)
    y = bisect(inds, last)
    if x == len(inds) or y == 0 or x == y:
        return 0
    if x == 0:
        return vals[y - 1]
    else:
        return vals[y - 1] - vals[x - 1]

def gene_health(gstr, gdlf, gdict, glenmax, first, left):
	gscore = 0
	for i, ch in enumerate(gstr):
		if glenmax[ch] == 0:
			continue
		for j in xrange(1, min(len(gstr) + 1 - i, glenmax[ch] + 1)):            
			chr = gstr[i:i+j]
			if '' in gdlf[chr]:
				gscore += gscore_word(gdict, chr, first, last)
			elif i + j < len(gstr) and gstr[i + j] not in gdlf[chr]:
				break
	return gscore

def preproc(genes, health):
    gdict, gdlf, glenmax = dict(), defaultdict(set), defaultdict(int)
    
    for i, x in enumerate(genes):
        inds, vals = gdict.get(x, ([], []))
        if vals == []:
            gdict[x] = ([i], [health[i]])
        else:
            gdict[x] = (inds + [i], vals + [vals[-1] + health[i]])
            
        if len(x) == 1:
                glenmax[x] = 1
        elif len(x) > glenmax[x[0]]:
            glenmax[x[0]] = len(x)
                
        ch, parent = '', x
        while parent:
            gdlf[parent].add(ch)
            ch, parent = parent[-1], parent[:-1]
            
    return gdlf, gdict, glenmax

if __name__ == '__main__':
	"""
    n = int(raw_input())

    genes = raw_input().rstrip().split()

    health = map(int, raw_input().rstrip().split())
	"""
	f = open(sys.argv[1])
	n = int(f.readline().rstrip())
	genes = f.readline().rstrip().split()
	health = map(int, f.readline().rstrip().split())
	gdlf, gdict, glenmax = preproc(genes, health)
	s = int(f.readline().strip())

    #s = int(raw_input())

	gscores = []
	for s_itr in xrange(s):
		#firstLastd = raw_input().split()
		firstLastd = f.readline().rstrip().split()

		first = int(firstLastd[0])

		last = int(firstLastd[1])

		d = firstLastd[2]
        
		gscore = gene_health(d, gdlf, gdict, glenmax, first, last)
		gscores.append(gscore)
        
	gscores.sort()
	print gscores[0], gscores[-1]
    

