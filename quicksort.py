#!/usr/bin/env python
"""
	Given an array partition them using the first element
	as a pivot.
"""
def partition(l):
	pivot = 0
	for i in range(1, len(l)):
		if l[i] <= l[pivot]:
			l[i], l[pivot] = l[pivot], l[i]
			expected_pivot = pivot + 1
			pivot = i
			if expected_pivot != pivot:
				l[expected_pivot], l[pivot] = l[pivot], l[expected_pivot]
				pivot = expected_pivot
	return l[:pivot], l[pivot], l[pivot+1:]

def quicksort(l):
	if len(l) <= 1:
		return l
	lo, pi, hi = partition(l)
	return quicksort(lo) + [pi] + quicksort(hi)
		
import sys
from random import randrange
try:
	num = int(sys.argv[1])
except:
	print "usage %s [size of array]" % sys.argv[0]
	exit(1)

l = [randrange(100) for i in range(num)]
print l
print quicksort(l)
