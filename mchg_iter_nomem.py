#!/usr/bin/env python
"""
	Given a set of coins like [25, 10, 5, 1] list all the ways
	in which you can make change for a given amount.
"""
import sys
try:
	amt = int(sys.argv[1])
except:
	print "usage : %s [amount to be changed]" % sys.argv[0]
	exit(1)

def make_change(amt, coins):
	Q = [([], amt, coins)]
	res = []
	while Q:
		tl, change, coins = Q.pop()
		for i, c in enumerate(coins):
			ntl = tl + [c]
			if change == c:
				res.append(ntl)
			elif change < c:
				continue
			else:
				Q.append((ntl, change - c, coins[i:])) 
	return res

res =  make_change(amt, [25, 10, 5, 1])
x = []
res.sort(key = lambda x: len(x))
for i in res:
	print i
