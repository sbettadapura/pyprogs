#!/usr/bin/env python
"""
	Given a set of coins, like [25, 10, 5, 1] lisr all ways in which
	a given amount can be changed.
"""
def make_graph(xsum, coins):
	g = {}
	Q = []
	Q.append(xsum)
	while Q:
		x = Q.pop()
		xset = set()
		for i in range(len(coins)):
			n = x - coins[i]
			if n >= 0:
				xset.add(n)
				Q.append(n)
		g[x] = xset	

	return g
if __name__ == "__main__":
	import sys
	from random import randrange
	from all_paths import all_paths
	
	try:
		xsum = int(sys.argv[1])
	except:
		print "usage %s [amount to be changed" % sys.argv[0]
		exit(1)

	print "ways of changing %d are:" % xsum
	g = make_graph(xsum, [25, 10, 5, 1])
	paths = all_paths(g, xsum, 0)
	e = []
	for path in paths:
		path.pop()
		for i in range(len(path) - 1):
			path[i] -= path[i+1]
		path.sort(reverse=True)
		e.append(path)
	e.sort(reverse=True)

	d = e[0]
	f = [d]
	for x in e[1:]:
		if d == x: continue
		d = x
		f.append(d)
	for i in f:
		print i
