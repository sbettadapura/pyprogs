#!/usr/bin/env python
"""
	A heap implementation using a root and pointer to children
	structure. Using an array is lot simpler and saner !
"""

from random import randrange
import sys
class Node:
	def __init__(self, val=None, parent=None, level=0):
		self.val = val
		self.left = self.right = None
		self.parent = parent
		self.next = None
		self.level = level
class Heap:
	def prnt(self):
		toprint = [self.mynode]
		level = self.mynode.level
		while toprint:
			x = toprint.pop(0)
			if x.level != level:
				print "\n"
				level = x.level
			str = "(%d)" % (x.val)
			sys.stdout.write(str)
			if x.left and x.left.val:
				toprint.append(x.left)	
			if x.right and x.right.val:
				toprint.append(x.right)	
		print "\n"
	def pop(self):
		node = self.mynode
		if node.val is None:
			return None
		while node:
			if node.left is None:
				child = node.right
			else:
				if node.right is None:
					child = self.left
				else:
					if node.left.val > node.right.val:
						child = node.left
					else:
						child = node.right	
			if child is None:
				val = node.val
				node.val = None
				return val
			else:
				node.val, child.val = child.val, node.val
				node = child
	def __init__(self, val):
		self.val = val
		self.left = self.right = None
	def add(self, val):
		if self.left is None:
			self.left = Heap(val)
		elif self.right is None:
			self.right = Heap(val)
		else:
			toinsert = self.determine()
			toinsert.insert(val)	
		myloc.val = val
		me = myloc
		parent = myloc.parent
		while parent:
			if parent.val < me.val:
				parent.val, me.val = me.val, parent.val
				me = parent
				parent = parent.parent
			else:
				break
if __name__ == "__main__":
	heap = Heap()
	lst = []
	n, rng = map(int, sys.argv[1:])
	while len(lst) < n:
		x = randrange(rng)
		if x in lst:
			continue
		lst.append(x)
		heap.add(x)
	#print lst
	#heap.prnt()
	x = heap.pop()
	while x is not None:
		#print x,	
		x = heap.pop()
	#print "\n"
