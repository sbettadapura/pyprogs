#!/usr/bin/env python
from random import choice
from random import randrange

def partition(a, k, v):
	left, right, mid = [], [], []
	n = len(a)
	for i in range(n):
		x = a[i]
		if x < v:
			left.append(x)
		elif x > v:
			right.append(x)
		else:
			mid.append(x)
	return (left, mid, right)

def kpart(a, k):
	n = len(a)
	v = choice(a)
	left, mid, right = partition(a, k, v)
	if left == [] and right == []:
		return mid[:k], mid[k:]
	elif len(right) == n - k:
		return left + mid, right
	elif len(right) > n - k:
		l, r =  kpart(right, k - len(left) - len(mid))
		return left + mid + l, r
	else:
		l, r =  kpart(left + mid, k)
		return l, r + right

if __name__ == "__main__":
	import sys
	n = int(sys.argv[1])
	a = [randrange(n * n) for i in range(n)]	
	print "a = ", a
	print "sorted(a) = ", sorted(a)
	l, r = kpart(a, (n + 1) / 2)	
	print l, r
	if n & 1:
		print "median of a = ", max(l)
	else:
		print "median of a = ", (max(l) + min(r)) * 1.0 / 2
