if __name__ == '__main__':
	def ext_neighbor(u, v):
		for x in nei[u]:
			if x != u: 
				nei[x] |= nei[v]
		nei[u] |= nei[v]

	import sys
	f = open(sys.argv[1])
	g_nodes, g_edges = map(int, f.readline().split())
	g_from = [0] * g_edges
	g_to = [0] * g_edges
	g_weight = [0] * g_edges

	for i in xrange(g_edges):
		g_from[i], g_to[i], g_weight[i] = map(int, f.readline().split())

	gd, es, nei = [], set(), dict()
	el, wl = [], []
	for x in range(1, g_nodes + 1):
		nei[x] = {x}
	for i, w in enumerate(g_weight):
		gd.append((w, g_from[i], g_to[i]))
		
	tree_weight = 0
	for w, u, v in sorted(gd):
		if nei[u] & nei[v]:
			continue

		es.add((u, v))
		el.append((u, v))
		wl.append(w)
		ext_neighbor(u, v)
		ext_neighbor(v, u)
		tree_weight += w
		if len(es) == g_nodes - 1:
			break
	print g_nodes, len(es)
	print tree_weight
	for i in range(1, g_nodes + 1):
		if len(nei[i]) != g_nodes:
			print i
