#!/usr/bin/emv python
import sys

def board_print():
	for i in range(SIZE):
		print "----" * SIZE
		print (("|%2i " * SIZE + "|") % tuple(board[i]))

	print "----" * SIZE

def outer_square(pt_x, pt_y, size, start):
	for i in range(size - 1):
		board[pt_y][pt_x + i] =	start + i
		board[pt_y + i][pt_x + size - 1] = start + size -  1 + i
		board[pt_y + size - 1][pt_x + size - 1 - i] = start + 2 * (size - 1) + i
		board[pt_y + size - 1 - i][pt_x] = start + 3 * (size - 1) + i

def spiral(pt_x, pt_y, size, start):
	if size == 0:
		return
	elif size == 1:
		board[pt_y][pt_x] = start
		return
	outer_square(pt_x, pt_y, size, start)
	spiral(pt_x + 1, pt_y + 1, size - 2, start + 4 * (size - 1))	

if __name__ == "__main__":
	try:
		int_arg = int(sys.argv[1])
	except:
		print "usage %s [size of square]" % sys.argv[0]
		exit(1)

	SIZE = int_arg
	board = [[0] * SIZE for i in range(SIZE)]
	spiral(0, 0, SIZE, 0)
	board_print()
