def powerset(l):
	if l == []:
		return [[]]
	x = l.pop()
	lwo = powerset(l)
	lwo += [y + [x] for y in lwo]
	return lwo

if __name__ == "__main__":
	import sys
	try:
		num = int(sys.argv[1])
	except:
		print "usage % [size of string]" % argv[0]
		exit(1)
	print sorted(powerset(list("abcdefghijklmnopqrstuvxyz"[:num])), key = len)
