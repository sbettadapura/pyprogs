#!/usr/bin/env python

arr = [1, 2, 4]
def steps(n):
	if n < 4:
		return arr[n - 1]
	return steps(n-1) + steps(n-2) + steps(n-3)

if __name__ == "__main__":
	import sys
	num = int(sys.argv[1])
	print num, steps(num)
	
