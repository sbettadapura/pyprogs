#!/usr/bin/env python
"""
	Given a width, produce all bit combinations.
	Given 3, 000, 001, 010, 011, 100, 101, 110, 111.
"""

def mask(cnt, prefix=''):
	if cnt == 0:
		print prefix
		return
	mask(cnt - 1, prefix + '0')
	mask(cnt - 1, prefix + '1')

if __name__ == "__main__":
	import sys
	mask(int(sys.argv[1]))
	try:
		mask(int(sys.argv[1]))
	except:
		print "usage %s [width of mask]" % sys.argv[0]
		exit(1)
