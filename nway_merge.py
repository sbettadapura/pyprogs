#!/usr/bin/env python

from heapq import heappop, heappush, heapify
def nway(a):
	while [] in a:
		a = a[:a.index([])] + a[a.index([]) + 1:]

	ax = [zip(reversed(a[k]), [k for j in range(len(a[k]))]) for k in range(len(a))]
	hax = [x.pop() for x in ax]
	heapify(hax)

	out = []
	while hax:
		min_num, stream = heappop(hax)
		out.append(min_num)
		if ax[stream]:
			heappush(hax, ax[stream].pop())

	assert(len(out) == sum([len(x) for x in a]))
	return out

if __name__ == "__main__":
	import sys
	from random import randrange
	num, size = map(int, sys.argv[1:])
	a = [[randrange(size * size) for i in range(randrange(size * 5))] for k in range(num)]
	for i in range(num):
		a[i].sort()
	#a = [[1, 2], [], [], [3, 4]]
	print a
	out = nway(a)
	print out	
