#!/usr/bin/env python2
import csv
import subprocess
import os
import sys
import time

import boto3

# Will use elastic ips for relay servers when set to true.
use_elastic_ip = True

#TODO the regions should be fetched each time this program is run.
aws_regions = {
    'ap-northeast-1': 'Asia Pacific (Tokyo)',
    'ap-northeast-2': 'Asia Pacific (Seoul)',
    'ap-south-1': 'Asia Pacific (Mumbai)',
    'ap-southeast-1': 'Asia Pacific (Singapore)',
    'ap-southeast-2': 'Asia Pacific (Sydney)',
    'ca-central-1': 'Canada (Central)',
    'eu-central-1': 'EU (Frankfurt)',
    'eu-west-1': 'EU (Ireland)',
    'eu-west-2': 'EU (London)',
    'sa-east-1': 'South America (Sao Paulo)',
    'us-east-1': 'US East (N. Virginia)',
    'us-east-2': 'US East (Ohio)',
    'us-west-1': 'US West (N. California)',
    'us-west-2': 'US West (Oregon)'
}


SSH_PORT = 37657
PROGRAM_NAME = ""
def Usage():
    print 'Usage: python %s \"credentials file name\" \"relay name\" \"region name\" \"SCOUT IP addr\"' % PROGRAM_NAME

def log(*msg):
    sys.stderr.write(' '.join(map(str, msg)))
    sys.stderr.write('\n')


def load_credentials(file_name):
#Get the key ID and the key from the "native" AWS CSV-formated credentials file.
#User name,Password,Access key ID,Secret access key,Console login link
#Administrator,,AKIAJP5BPJ6PSUZ5RIAA,Zgsj0ubfz3M22lULT326Y/ozLHgCBxhULr7m4ijn,https://522654139092.signin.aws.amazon.com/console
#The values of the first row of the file will be used as field names.
#In this case, they will be the five parameters:
# "User name"
# "Password"
# "Access key ID"
# "Secret access key"
# "Console login link"
    with open(file_name) as csvFile:
        reader = csv.DictReader(csvFile)
        for row in reader:
            return (row['Access key ID'], row['Secret access key'])


def make_file_dirs(name):
    directory_name = os.path.dirname(name)
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
        os.chmod(directory_name, 0o700)


class Server(object):

    def __init__(self, Name, Region, Ec2=None, Key=None, Security=None, Instance=None, DataDir=None):
        self.name = Name
        self.region = Region
        self.ec2 = Ec2
        self.key = Key
        self.key_file = os.path.join(DataDir, '.relay', self.name + '.pem')
        if type(self.key).__name__ == 'ec2.KeyPair':
            self.save_key()
        self.security = Security
        self.instance = Instance
        self.ip_address = None
        if self.instance:
            self.ip_address = self.instance.public_ip_address

    def reference(self):
        return '%s,%s' % (self.name, self.region)

    def delete(self):
        if self.instance:
            log(self.instance, 'termate')
            self.instance.terminate()
            self.instance.wait_until_terminated()
            self.release_address(self.ip_address)
            if os.path.exists(self.key_file):
                os.remove(self.key_file)
        if self.security:
            log(self.security, 'delete')
            self.security.delete()
        if self.key:
            log(self.key, 'delete')
            self.key.delete()

    def release_address(self, ip_address):
        client = self.ec2.meta.client

        response = client.describe_addresses(Filters=[
            {'Name': 'domain', 'Values': ['vpc']},
            {'Name': 'public-ip', 'Values': [ip_address]},
        ])

        for i in response['Addresses']:
            client.release_address(AllocationId=i['AllocationId'])

    def save_key(self):
        make_file_dirs(self.key_file)
        with open(self.key_file, 'w') as file_obj:
            file_obj.write(self.key.key_material)
        os.chmod(self.key_file, 0o700)
        log('saved', self.key_file)

    def send_command(self, *args):
        command = ["/usr/bin/env", "ssh",
                   '-oLogLevel=error', '-oStrictHostKeyChecking=no', '-oUserKnownHostsFile=/dev/null',
                   "-i", self.key_file, "ubuntu@" + self.ip_address]
        for _ in range(3):
            try:
                return subprocess.check_output(command + list(args))
            except subprocess.CalledProcessError:
                log('\tRetry: ' + ' '.join(args))
                time.sleep(30)
        time.sleep(30)
        return subprocess.check_output(command + list(args))

    def wait_for_ssh(self):
        while True:
            try:
                log('trying to connect')
                subprocess.check_output(["uname", "-a"])
                break
            #TODO make this exception explicit... JRZ
            except Exception:
                time.sleep(1)
        log('success')

    def make_proxy(self, forward_to_ip):
        self.send_command("sudo apt-get --assume-yes update")
        self.send_command("echo net.ipv4.ip_forward=1 >/tmp/90-ip-forward.conf")
        self.send_command("sudo cp /tmp/90-ip-forward.conf /etc/sysctl.d/")
        self.send_command("sudo sysctl net.ipv4.ip_forward=1")
        self.send_command("sudo iptables -t nat -A PREROUTING -p udp --dport 4500 -j DNAT --to-destination %s:4500" % forward_to_ip)
        self.send_command("sudo iptables -t nat -A PREROUTING -p udp --dport 500 -j DNAT --to-destination %s:500" % forward_to_ip)
        self.send_command("sudo iptables -t nat -A PREROUTING -p udp --dport 68 -j DNAT --to-destination %s:68" % forward_to_ip)
        self.send_command("sudo iptables -t nat -A POSTROUTING -j MASQUERADE")
        # The following two commands enable the apt-get install of iptables iptables-persistent to be non-interactive:
        self.send_command("echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections")
        self.send_command("echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections")
        # iptables-persistent saves all of the current iptables rules during installation
        self.send_command("sudo DEBIAN_FRONTEND=noninteractive apt-get --assume-yes install iptables-persistent")
        # on Ubuntu 16.04 Server use the command below to force a save.
        # self.send_command("sudo netfilter-persistent save")
        # Show the table
        # self.send_command("sudo iptables -L -t nat")

        # After this we will only be able to login on the new ssh port.
        self.send_command("sudo sed -i 's/Port 22/Port %d/g' /etc/ssh/sshd_config" % SSH_PORT)
        self.send_command("sudo systemctl restart sshd")


class RegionBuilder(object):

    def __init__(self, aws_access_key_id, aws_secret_access_key, region, data_dir):
        self.region = region
        self.data_dir = data_dir
        self.ec2 = boto3.resource('ec2',
                                  aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key,
                                  region_name=region)

    def get_key(self, name):
        self.ec2.KeyPair(name).delete()
        return self.ec2.create_key_pair(KeyName=name)

    def get_security(self, name):
        for i in self.ec2.security_groups.all():
            if i.group_name == name:
                i.delete()
        group = self.ec2.create_security_group(GroupName=name, Description=name)
        group.authorize_ingress(IpProtocol='tcp', FromPort=22, ToPort=22, CidrIp="0.0.0.0/0")
        group.authorize_ingress(IpProtocol='tcp', FromPort=SSH_PORT, ToPort=SSH_PORT, CidrIp="0.0.0.0/0")
        group.authorize_ingress(IpProtocol='udp', FromPort=4500, ToPort=4500, CidrIp="0.0.0.0/0")
        group.authorize_ingress(IpProtocol='udp', FromPort=500, ToPort=500, CidrIp="0.0.0.0/0")
        group.authorize_ingress(IpProtocol='udp', FromPort=68, ToPort=68, CidrIp="0.0.0.0/0")
        return group

    def get_latest_image(self):
        images = self.ec2.images.filter(
            Owners=['099720109477'],
            Filters=[
                {'Name': 'virtualization-type', 'Values': ['hvm']},
                {'Name': 'root-device-type', 'Values': ['ebs']},
                {'Name': 'architecture', 'Values': ['x86_64']},
                {'Name': 'name', 'Values': ['ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*']}
            ])
        images = sorted(images, key=lambda r: r.creation_date)
        if not images:
            raise Exception('No AMI images found')

        log(images[-1], 'latest ami')
        # Sorted by date this will be the most recent.
        return images[-1]

    def make_server(self, name, region):
        address = None
        if use_elastic_ip:
            address = self.ec2.meta.client.allocate_address()
        ami = self.get_latest_image()
        key = self.get_key(name)
        security = self.get_security(name)

        instance = self.ec2.create_instances(
            ImageId=ami.id,
            InstanceType='t2.nano',
            MaxCount=1,
            MinCount=1,
            UserData='',
            KeyName=key.name,
            SecurityGroupIds=[security.id],
            TagSpecifications=[{'ResourceType': 'instance', 'Tags': [
                {'Key': 'Name', 'Value': name},
                {'Key': 'Type', 'Value': 'relay'},
            ]}],
        )[0]

        log(instance, 'wait_until_running')
        instance.wait_until_running()
        if use_elastic_ip:
            self.ec2.meta.client.associate_address(InstanceId=instance.id,
                                                   AllocationId=address["AllocationId"])
        instance.load()
        return Server(name, region, Ec2=self.ec2, Key=key, Security=security, Instance=instance, DataDir=self.data_dir)

    def lookup_key(self, name):
        return self.ec2.KeyPair(name)

    def lookup_security(self, name):
        for i in self.ec2.security_groups.all():
            if i.group_name == name:
                return i
        return None

    def lookup_instances(self, name):
        instances = self.ec2.instances.filter(
            Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])

        rtn = []
        for i in instances:
            tags = {j['Key']: j['Value'] for j in i.tags or []}
            if tags.get('Name') == name:
                rtn.append(i)
        return rtn

    def find_server(self, name):
        instances = self.lookup_instances(name)
        if len(instances) == 0:
            return None
        if len(instances) > 1:
            raise Exception('To many instances found')

        key = self.lookup_key(name)
        security = self.lookup_security(name)

        return Server(name, self.region, Ec2=self.ec2, Key=key, Security=security, Instance=instances[0], DataDir=self.data_dir)

    def lookup_instances_prefix(self, name):
        instances = self.ec2.instances.filter(
            Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])

        rtn = []
        for i in instances:
            tags = {j['Key']: j['Value'] for j in i.tags or []}
            if tags.get('Name').startswith(name):
                rtn.append(i)
        return rtn

    def find_servers_prefix(self, name):
        rtn = []
        for i in self.lookup_instances_prefix(name):
            tags = {j['Key']: j['Value'] for j in i.tags or []}
            name = tags.get('Name')
            key = self.lookup_key(name)
            security = self.lookup_security(name)
            rtn.append(Server(name, self.region, Ec2=self.ec2, Key=key, Security=security, Instance=i, DataDir=self.data_dir))
        return rtn


class Builder(object):

    def __init__(self, CREDENTIALS_FILE_name, data_dir):
        self.aws_access_key_id, self.aws_secret_access_key = load_credentials(CREDENTIALS_FILE_name)
        self.data_dir = data_dir

    def new_relay(self, RELAY_NAME, region):
        region_builder = RegionBuilder(self.aws_access_key_id, self.aws_secret_access_key, region, self.data_dir)
        return region_builder.make_server(RELAY_NAME, region)

    def new_relay_pool(self, base_name, ip_address, regions):
        rtn = []
        for i, region in enumerate(regions):
            name = '%s-%x' % (base_name, int(time.time()))
            log('\nCreating relay %s, %d of %d in %s' % (name, i + 1, len(regions), region))
            relay = self.new_relay(name, region)
            relay.make_proxy(ip_address)
            rtn.append(relay)
            log('finished creating relay', relay.reference(), relay.ip_address)
        return rtn

    def find_relay_pool(self, name, regions):
        rtn = []
        for _, region in enumerate(regions):
            region_builder = RegionBuilder(self.aws_access_key_id, self.aws_secret_access_key, region, self.data_dir)
            rtn += region_builder.find_servers_prefix('%s-' % name)
        return rtn

    def get_all_regions(self):
        return aws_regions.keys()

    def relay_ips(self, relay_list):
        return [i.ip_address for i in relay_list]

    def relay_tags(self, relay_list):
        return [{'Key': 'relay-%d' % k, 'Value': v.reference()} for k, v in enumerate(relay_list)]

    def relay_references(self, tags):
        rtn = []
        for i in tags:
            if i.get('Key', '').startswith('relay-'):
                rtn.append(i.get('Value'))
        return rtn

    def relay_servers(self, relays):
        rtn = []
        for i in relays:
            name, region = i.split(',')
            region_builder = RegionBuilder(self.aws_access_key_id, self.aws_secret_access_key, region, self.data_dir)
            rtn.append(region_builder.find_server(name))
        return rtn

if __name__ == '__main__':

    ARGUMENT_COUNT_REQUIREMENT = 4
    argument_counter = len(sys.argv)
    #Get the program name even when the command line syntax is violated.
    PROGRAM_NAME = sys.argv[0]
    if argument_counter == 1:
        Usage()
        sys.exit(1)
    argument_counter = argument_counter -1 #Ignore the program name
    if ARGUMENT_COUNT_REQUIREMENT != argument_counter:
        print "%d arguments were supplied." % len(sys.argv)
        print "argument counter = %d" % argument_counter
        Usage()
        sys.exit(1)
    CREDENTIALS_FILE = sys.argv[1]
    RELAY_NAME = sys.argv[2]
    REGION_NAME = sys.argv[3]
    SCOUT_IP_ADDRESS = sys.argv[4]
    print "%s, credentials file: \"%s\", RELAY_NAME: \"%s\", REGION_NAME: \"%s\", SCOUT_IP_ADDRESS: \"%s\"" %(PROGRAM_NAME, CREDENTIALS_FILE, RELAY_NAME, REGION_NAME, SCOUT_IP_ADDRESS)

    WORK_DIRECTORY="/tmp"
    builder = Builder(CREDENTIALS_FILE, WORK_DIRECTORY)
    relay = builder.new_relay(RELAY_NAME, REGION_NAME)
    print "Private key file: \"%s\"." % relay.key_file
    print "Public IP address: \"%s\"." % relay.ip_address
    relay.make_proxy(SCOUT_IP_ADDRESS)
    print "SSH access port: \"%d\"" % SSH_PORT
    print 'done'
