#!/usr/bin/env python
"""
	Celebrity problem
	Everyone knows a celebrity but he knows nobody.
	If g[][] is an adjacency matrix, g[c][i] is False for all i
	and g[i][c] is True for all i where 0 <= i < len(g).
"""


def celebrity(g):
	n = len(g)
	celeb = set(range(n))
	u = celeb.pop() 
	while celeb:
		v = celeb.pop()
		if g[u][v]: u = v

	for v in range(n):
		if u == v: continue
		if g[u][v] or not g[v][u]:
			return None
	else:
		return u

if __name__ == "__main__":

	from random import randrange
	import sys

	try:
		n = int(sys.argv[1])
	except:
		print "usage: %s [number of people]" % sys.argv[0]
		exit(1)

	g = [[randrange(2) for i in range(n)] for i in range(n)]

	# pick a celebrity
	c = randrange(n)
	for i in range(n):
		g[i][c] = True
		g[c][i] = False

	print "celebrity chosen is ", c

	print "celebrity discovered is: ", celebrity(g)
