#!/usr/bin/env python

def time_to_sec(line):
	month_to_int = {'Jan':1,'Feb':2,'Mar':3,'Apr':4,'May':5,'Jun':6,'Jul':7,'Aug':8, 'Sep':9, 'Oct':10, 'Nov':11, 'Dec':12}

	created_at = re.match(r".*?created_at\":\"(.*?)\",\"id\".*", line)
	day, month, date, time, unkn, year = created_at.group(1).split()
	year = int(year)
	date = int(date)
	hh, mm, ss = map(int, time.split(':'))
	julian_day = datetime.date.toordinal(datetime.date(year, month_to_int[month], date))
	creation_time = julian_day * 24 * 3600 + hh * 3600 + mm * 60 + ss
	return creation_time

def extract_hashtags(line):
	hashtags = []
	x = re.match(r".*hashtags\":\[({\"text\".*}).*\],\"urls.*", line)
	if x is None:
		return hashtags
	y = x.group(1)
	for z in  re.findall(r"{\"text\":\".*?\",.*?}", y):
		h = re.match(r".*\"text\":\"(.*?)\",.*?}", z)
		hashtags.append(h.group(1))
	return hashtags

def add_twit(degree, twit_heap, twit):
	heappush(twit_heap, twit)
	hashtags = twit[1]
	for i in hashtags:
		if len(hashtags) > 1:
			degree[i] += len(hashtags) - 1

def delete_twit(degree, twit):
	hashtags = twit[1]
	for i in hashtags:
		if i in degree:
			degree[i] -= len(hashtags) - 1
			if degree[i] == 0:
				del degree[i]
def delete_older_twits(degree, twit_heap, twit):
	oldest_ts = int(twit[0]) - 60
	while twit_heap:
		tmp = heappop(twit_heap)
		if int(tmp[0]) >= oldest_ts:
			heappush(twit_heap, tmp)
			break
		else:
			delete_twit(degree, tmp)

def report_degree(degree):
	vsum = 0.0
	for k in degree.keys():
		vsum += degree[k]
	if len(degree) == 0:
		print 0.00
	else:
		print "%.2f" % (round(vsum / len(degree), 2))

def process_twit(twit):
	global latest_twit
	ts = twit[0]
	hashtags = twit[1]
	if latest_twit is None or ts <= latest_twit[0] + 60 or ts >= latest_twit[0] - 60:
		delete_older_twits(degree, twit_heap, twit)
		add_twit(degree, twit_heap, twit)
		latest_twit = twit
		report_degree(degree)

if __name__ == "__main__":
	import sys
	import re
	import datetime
	from collections import defaultdict
	from heapq import heappush, heappop
	twit_heap = []
	latest_twit = None
	degree = defaultdict(int)

	fd = open(sys.argv[1])
	for line in fd.readlines():
		if not "hashtags" in line:
			continue
		line = line.strip()
		creation_time = time_to_sec(line)
		hashtags = extract_hashtags(line)
		if not hashtags:
			continue
		process_twit((creation_time, hashtags))
