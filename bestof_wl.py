#!/usr/bin/env python
"""
	Given a best-of-n game series, print all possible ways in which the
	series can end. A best of seven can end in WWWW, WWWLLLW, ... LLLWWWW.
"""

def comb(str, cnt, prefix=""):
	if cnt == 0:
		return [prefix]
	y = []
	for i in range(len(str)):
		x = comb(str[i+1:], cnt - 1, prefix + str[i])	
		y.extend(x)
	return y
if __name__ == "__main__":
	import sys
	try:
		num = int(sys.argv[1])
	except:
		print "usage %s [number of games in series" % sys.argv[0]
		exit(1)

	pat = "abcdefghijklmnopqrstuvwxyz"[:num]
	for c in comb(pat, num / 2 + 1):
		x = ["LW"[int(x in c)]for x in pat]
		while x[-1] == "L":
			x.pop()
		print ''.join(x)
