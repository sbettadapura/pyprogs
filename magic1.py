#!/usr/bin/env python

from collections import defaultdict
from operator import add

def print_magic(a, cost):
	print cost
	for i in range(3):
		print tuple(a[i])

def replace_num(a, i, j, idx):
	curr = a[i][j]
	cand = 15 - (sum(a[i]) - curr)
	if cand > 0 and cand < 10 and idx[cand] == 0:
		idx[curr] -= 1
		a[i][j] = cand
		idx[cand] = 1
		cost = abs(curr - cand)
		return cost
	return 0

def replace_num_col(a, i, j, idx):
	curr = a[i][j]
	cand = 15 - (sum(a[i][j] for i in range(3))) + curr
	if cand > 0 and cand < 10 and idx[cand] == 0:
		idx[curr] -= 1
		a[i][j] = cand
		idx[cand] = 1
		cost = abs(curr - cand)
		return cost
	return 0

def deduplicate(a, idx):
	cost = 0
	for i in range(3):
		for j in range(3):
			if idx[a[i][j]] > 1:
				cost +=  replace_num(a, i, j, idx)
	return cost

def miss(a):
	idx = defaultdict(int)
	for i in range(3):
		for j in range(3):
			idx[a[i][j]] += 1
	missing = any([idx[i] == 0 for i in range(1, 10)])
	return missing, idx
	
def row_adjust(a, idx):
	rowdelta = [15 - sum(a[i]) for i in range(3)]
	cost = 0
	for i in range(3):	
		if rowdelta[i] != 0:
			for j in range(3):
				cost += replace_num(a, i, j, idx)
	return cost

def col_adjust(a, idx):
	coldelta = [15 - sum(a[i][j] for i in range(3)) for j in range(3)]
	cost = 0
	for j in range(3):	
		if coldelta[j] != 0:
			for i in range(3):
				cost += replace_num_col(a, i, j, idx)
	return cost

def check_magic(a):
	rowdelta = [15 - sum(a[i]) for i in range(3)]
	coldelta = [15 - sum(a[i][j] for i in range(3)) for j in range(3)]
	return sum(rowdelta) + sum(coldelta) == 0

def col_intra_adjust(a, idx):
	return 0

def row_intra_adjust(a, idx):
	cost = 0
	for col in range(3):
		csum = sum([a[i][col] for i in range(3)])
		if csum == 15:
			continue
		for row in range(3):
			if col == 0:
				tmp0, tmp1 = a[row][0], a[row][1]
				a[row][0], a[row][1] = a[row][1], a[row][0]
				if check_magic(a):
					cost += abs(a[row, 0] - tmp0) + abs(a[row, 1] - tmp1)
					return cost
				else:	
					a[row][0], a[row][1] = tmp0, tmp1
					tmp0, tmp2 = a[row][0], a[row][2]
					a[row][0], a[row][2] = a[row][2], a[row][0]
					if check_magic(a):
						cost += abs(a[row][0] - tmp0) + abs(a[row][2] - tmp2)
						return cost
					else:
						a[row][0], a[row][2] = tmp0, tmp2
			elif col == 1:
				tmp1, tmp2 = a[row][1], a[row][2]
				a[row][1], a[row][2] = a[row][2], a[row][1]
				if check_magic(a):
					cost += abs(a[row][1] - tmp1) + abs(a[row][2] - tmp2)
					return cost
				else:	
					a[row][1], a[row][2] = tmp1, tmp2
		return 0

def magic(a, cost = 0):
	missing, idx = miss(a)
	cost = 0
	while missing:
		newcost = deduplicate(a, idx)
		if newcost == 0:
			break
		cost += newcost
		missing, idx = miss(a)

	while missing:
		newcost = row_adjust(a, idx)
		if newcost == 0:
			break
		cost += newcost
		missing, idx = miss(a)

		newcost = col_adjust(a, idx)
		if newcost == 0:
			break
		cost += newcost
		missing, idx = miss(a)

	while missing:	
		newcost = deduplicate(a, idx)
		if newcost == 0:
			break
		cost += newcost
		missing, idx = miss(a)

	rows_ok = all([15 - sum(a[i])  == 0 for i in range(3)])
	cols_ok = all([15 - sum(a[i][j] for i in range(3))  == 0 for j in range(3)])
	if cols_ok and rows_ok:
		return cost, a
	elif rows_ok:
		cost += row_intra_adjust(a, idx)
	else:
		cost += col_intra_adjust(a, idx)
	return cost, a

if __name__ == "__main__":
	square = [[7, 4, 9], [2, 6, 2], [2, 8, 1]]
	print magic(square)
	square = [[2, 2, 7], [8, 6, 4], [1, 2, 9]]
	print magic(square)
	square = [[4, 8, 2], [4, 5, 7], [6, 1, 6]]
	print magic(square)
	square = [[2, 7, 6], [8, 5, 1], [4, 4, 6]]
	print magic(square)
