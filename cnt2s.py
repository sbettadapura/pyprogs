#!/usr/bin/env python

def twos(n):
	num = 0
	while n:
		if n % 10 == 2:
			num += 1
		n /= 10
	return num
	
def count2s_brute(n):
	num2s = 0
	for i in range(2, n+1):
		num2s += twos(i)
	return num2s

def lower(pow10):
	num = 0
	pow10 /= 10
	while pow10:
		pow10 /= 10
		num += 1
	return num * 10 ** (num - 1)	

def num2s_sub100(n):
	num2s = 0
	unit = n % 10
	pow10 = n / 10
	if unit > 1:
		num2s += 1
	if pow10 == 1:
		num2s += 1
	elif pow10 == 2:
		num2s += (unit + 3)
	else:
		num2s += (pow10 + 11)
	return num2s
def count2s_algo(n):
	num2s = 0
	procd = n % 10
	if procd > 1:
		num2s = 1
	pos_num2s = 1
	n /= 10
	carry_over = 1
	while n:
		curr = n % 10
		if curr == 1:
			num2s += carry_over
		elif curr == 2:
			if procd != 0:
				num2s += procd + 1 + 2 * carry_over
			else:
				num2s += carry_over
		elif curr != 0:
			num2s += curr * carry_over + pos_num2s * 10
		procd += curr * pos_num2s * 10
		pos_num2s *= 10
		n /= 10
		carry_over = carry_over * 10 + pos_num2s
	return num2s

def cnt2s(n):
	pow10 = 1
	twos_in_pow10 = 1
	this_digit = n % 10
	if this_digit > 1:
		num2s = 1
	else:
		num2s = 0
	n /= 10
	partial_num = this_digit

	while n:
		pow10 *= 10
		this_digit = n % 10
		num2s += this_digit * twos_in_pow10
		if this_digit > 2:
			num2s += pow10
		elif this_digit == 2:
			num2s += partial_num + 1

		twos_in_pow10 *= 10
		twos_in_pow10 += pow10
		n /= 10
		partial_num += this_digit * pow10

	return num2s

if __name__ == "__main__":
	import sys
	num = int(sys.argv[1])
	print num, count2s_algo(num)
	print num, cnt2s(num)
