#!/usr/bin/env python

def sqrt(n, tolerance):
	if n > 1:
		low = 0
		high = n
	else:
		low = n
		high = 1
	while True:
		mid = (low + high) / 2
		diff = n - mid * mid
		if abs(diff) < tolerance:
			return mid
		if diff > 0:
			low = mid
		else:
			high = mid

if __name__ == "__main__":
	import sys
	n = float(sys.argv[1])
	print sqrt(n, 0.0005)
