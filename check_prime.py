#!/usr/bin/env python

prime = [3]
def check_prime(n):
	for i in range(1, n + 1, 2):
		if i * i < n:
			if prime[-1] < i:
				for j in range(prime[-1] + 2, i + 1, 2):
					if all([j % x != 0 for x in prime]):
						prime.append(j)
		else:
			print prime
			return all([n % x != 0 for x in prime])
if __name__ == "__main__":
	import sys
	try:
		num = int(sys.argv[1])
	except:
		print "usage %s [number to check for prime]" % sys.argv[0]
		exit(-1)
	print num, " is prime is ", check_prime(num)
	print prime
