#!/usr/bin/env python

def check_replace(str1, str2):
	found = False
	for i in range(len(str1)):
		if str1[i] != str2[i]:
			if found:
				return False
			else:
				found = True
	else:
		return True
def check_insert_delete(str1, str2):
	i = 0
	j = 0
	found = False
	while i < len(str1) and j < len(str2):
		if str1[i] != str2[j]:
			if found:
				return False
			elif len(str1) > len(str2):
				i += 1
				found = True
			else:
				j += 1
				found = True
		else:
			i += 1
			j += 1
	else:
		return True

def oneaway(str1, str2):
	if len(str1) == len(str2):
		return check_replace(str1, str2)
	elif abs(len(str1) - len(str2)) > 1:
		return False
	else:
		return check_insert_delete(str1, str2)

if __name__ == "__main__":
	import sys
	str1 = sys.argv[1]
	str2 = sys.argv[2]
	print oneaway(str1, str2)	
