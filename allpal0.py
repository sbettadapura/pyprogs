#!/usr/bin/env python
"""
	Given a string like 'abbccd' find all palindromes
"""
def allpal(redu, nonredu):
	if redu == "":
		return list(nonredu)

	x = allpal(redu[:-1], nonredu)
	z = x[:]
	if redu[:-1] == "":
		x = []
	for i in z:
		y = redu[-1] + i + redu[-1]
		x.append(y)
	return x

def perm(str, cnt=-1, prefix=""):
	if cnt == -1:
		cnt = len(str)
	if cnt == 0:
		return [prefix]
	x = []
	for i in range(len(str)):
		x += perm(str[:i] + str[i+1:], cnt - 1, prefix + str[i])
	return x

import sys
try:
	rest = sys.argv[1]
except:
	print "usage %s [palindrome source]" % sys.argv[0]

redu = ""
for c in rest:
	if rest.count(c) > 1:
		rest = rest.replace(c, '', 2)
		redu += c	

for p in  perm(redu):
	print allpal(p, rest)
