#!/bin/python

import os
import sys

#
# Complete the buildPalindrome function below.
#
def checkpal(asub, b):
	asubl = list(asub)
	arev0 = ''.join([y for y in reversed(asubl)])
	res = ''
	if arev0 in b:
		res = asub + arev0
	elif len(arev0) > 1:
		arev1  = arev0[1:]
		if arev1 in b:
			res = asub + arev1
	return res

def buildPalindrome(a, b):
    #
	max_res = ''
	for i in xrange(len(a)):
		for par in (a[:len(a) - i], a[i:]):
			res = checkpal(par, b)
			if len(res) > len(max_res):
				max_res = res
	if max_res == '':
		max_res = str(-1)
	return max_res
if __name__ == '__main__':
	f = open(sys.argv[1])
	#fptr = open(os.environ['OUTPUT_PATH'], 'w')
	#t = int(raw_input())
	t = int(f.readline().rstrip())
	for t_itr in xrange(t):
		#a = raw_input()
		a = f.readline().rstrip()
		#b = raw_input()
		b = f.readline().rstrip()
		#fptr.write(result + '\n')
		result = buildPalindrome(a, b)
		print result
	#fptr.close()

