#!/usr/bin/env python
"""
	Given an array find the slice with greatest sum.
	[12, 2, 15, -4, 18] [15, -4], 18] is the answer.
	If all numbers are negative the empty [] is the solution.
"""

def greatest_slice_brute(L):
	n = len(L)
	return max([sum(L[i:j]) for i in range(n) for j in range(i+1, n+1)])

def greatest_slice_linear(L):
	curr_seq = []
	max_seq = []
	n = len(L)
	for i in range(n):
		if L[i] < 0:
			if sum(curr_seq) > sum(max_seq):
				max_seq = curr_seq[:]
			if sum(curr_seq) + L[i] < 0:
				curr_seq = []
			else:
				curr_seq.append(L[i])	
		else:
			curr_seq.append(L[i])	

	if sum(curr_seq) > sum(max_seq):
		max_seq = curr_seq[:]

	return max_seq

if __name__ == "__main__":
	import sys
	from random import randrange
	try:
		size = int(sys.argv[1])
	except:
		print "usage: %s [size of array]" % sys.argv[0]
		exit(1)
	L = [randrange(20 * size) * [-1, 1][randrange(2)] for i in range(size)]
	#L = [-40, -61, 159, -71, -38, -53, 135, 89]
	#L = [-126, -110, -35, -37, -131, 89, 92, -142]
	#L = [128, -28, 80, -130, -155, 10, -37, 65]
	print "array = ", L
	max_seq = greatest_slice_linear(L)
	print "nax seq = ", max_seq, "max_sum = ", sum(max_seq)
	max_seq = greatest_slice_brute(L)
	print "nax seq brute = ", max_seq
