#!/usr/bin/env python
"""
	Given an array of numbers print the two highest ones.
"""

from random import randrange
import sys
try:
	n = int(sys.argv[1])
except:
	print "usage: %s [elements in array]" % sys.argv[0]
	exit(1)

L = [randrange(n * 10) for x in range(n)]
print L

def twohigh(L):
	l,h = L[:2]

	if l > h:
		l, h = h, l

	for x in L[2:]:
		if x > h:
			l = h
			h = x
		elif l < x:
			l = x
	return l, h

l, h  = twohigh(L)
print "two highest = ", h, l
