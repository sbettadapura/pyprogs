#!/usr/bin/env python
from bisect import bisect

class Tree:
	def __init__(self, root, left = None, right = None):
		self.root = root
		self.left = left
		self.right = right

def preTobst(pre):
	if not pre:
		return None
	if len(pre) == 1:
		return Tree(pre[0])
	root = pre[0]
	idx = bisect(pre, root)
	return Tree(pre[0], preTobst(pre[1:idx]), preTobst(pre[idx:]))

if __name__ == "__main__":
	def print_tree(tree):
		if tree.left is not None:
			print_tree(tree.left)
		print tree.root,
		if tree.right is not None:
			print_tree(tree.right)
	def print_norec_preorder(tree):
		stk = [tree]
		while stk:
			curr = stk.pop()
			if curr:
				print curr.root,
				if curr.right:
					stk.append(curr.right)
				if curr.left:
					stk.append(curr.left)
		print "\n",

	def print_post_order(tree):
		if tree.left:
			print_post_order(tree.left)
		if tree.right:
			print_post_order(tree.right)
		print tree.root,

	PRINT, PROCESS = range(2)
	def print_post_order_norec(tree):
		stk = [(PROCESS, tree)]	
		while stk:
			pro, curr = stk.pop()
			if pro == PROCESS:
				stk.append((PRINT, curr))	
				if curr.right:
					stk.append((PROCESS, curr.right))
				if curr.left:
					stk.append((PROCESS, curr.left))
			elif pro == PRINT:
				print curr.root,	

	def print_norec_inorder(tree):
		stk = []
		curr = tree
		while stk or curr:
			if curr:
				stk.append(curr)
				curr = curr.left
			else:
				x = stk.pop()
				print x.root,
				if x.right:
					curr = x.right
		print "\n",

	pre = [43, 23, 37, 29, 31, 41, 47, 53]	
	tree = preTobst(pre)
	print_tree(tree)
	print "\n",
	print_norec_inorder(tree)
	print_norec_preorder(tree)
	print_post_order(tree)
	print "\n",
	print_post_order_norec(tree)
