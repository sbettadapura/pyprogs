#!/usr/bin/env python
"""
	A simple decorator function to call numbnerr of times
	a function is called
"""

def call_counter(func):
	def helper(*args, **kwargs):
		helper.calls += 1
		return func(*args, **kwargs)
	helper.calls = 0
	return helper

@call_counter
def succ(x):
	return x + 1	

if __name__ == "__main__":
	print succ.calls
	for i in range(5):
		print "i in loop = ", i
		succ(i)
	print "i = ", i
	print succ.calls
