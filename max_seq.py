#!/usr/bin/env python
"""
	Given an array of (unsorted) numbers pick out the 
	largest run.
"""
import sys
from random import randrange
try:
	num = int(sys.argv[1])
except:
	print "usage %s [size of sequence]" % sys.argv[0]

arr = [randrange(num * 20) for i in range(num)]

curr_seq = max_seq = arr[:1]

for i in range(1, num):
	if arr[i] >= curr_seq[-1]:
		curr_seq.append(arr[i])
	else:
		if len(curr_seq) > len(max_seq):
			max_seq = curr_seq[:]
		curr_seq = arr[i:i+1]

print "arr = ", arr
print "max sequence = ", max_seq
