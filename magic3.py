#!/usr/bin/env python

from collections import defaultdict
from operator import add
from random import randrange

def print_magic(a, cost):
	print cost
	for i in range(3):
		print tuple(a[i])

def swap_nums(a, cand, curr, i, j, idx):
	if cand > 0 and cand < 10 and idx[cand] == 0:
		idx[curr] -= 1
		a[i][j] = cand
		idx[cand] = 1
		cost = abs(curr - cand)
		return cost
	else:
		return 0

def replace_num(a, i, j, idx):
	curr = a[i][j]
	cand = 15 - (sum(a[i]) - curr)
	return swap_nums(a, cand, curr, i, j, idx)

def replace_num_col(a, i, j, idx):
	curr = a[i][j]
	cand = 15 - (sum(a[i][j] for i in range(3))) + curr
	return swap_nums(a, cand, curr, i, j, idx)

def find_num(a, t):
	found = []
	for i in range(3):
		for j in range(3):
		 if a[i][j] == t:
			found.append((i, j))
	return found

def deduplicate_force(a, idx):
	target, create = [], []
	for i in range(1, 10):
		if idx[i] > 1:
			target.append(i)
		elif idx[i] == 0:
			create.append(i)

	for t in target:
		for x, y in find_num(a, t):
			if sum(a[x]) != 15 and sum(a[i][y] for i in range(3)) != 15:
				break

	for c in create:
		if sum(a[x]) + c - t == 15 or sum(a[i][y] for i in range(3)) + c - t == 15:
			break
			
	idx[t] -= 1
	a[x][y] = c
	idx[c] = 1
	return abs(t - c)

def ducks_in_row(a, idx):
	pass

def ducks_in_col(a, idx):
	neighbors = {9:[5, 1, 4, 2], 
				 8:[6, 1, 5, 2], 
				 7:[6, 2, 5, 3], 
				 6:[8, 1, 7, 2, 5, 4], 
				 5:[9, 1, 8, 2, 7, 3, 6, 4], 
				 4:[9, 2, 8, 3, 6, 5], 
				 3:[8, 4, 7, 5], 
				 2:[9, 4, 8, 5, 7, 6], 
				 1:[9, 5, 8, 6]}

	pass

def deduplicate(a, idx):
	cost = 0
	for i in range(3):
		for j in range(3):
			if idx[a[i][j]] > 1:
				cost +=  replace_num(a, i, j, idx)
	return cost

def miss(a):
	idx = defaultdict(int)
	for i in range(3):
		for j in range(3):
			idx[a[i][j]] += 1
	missing = any([idx[i] == 0 for i in range(1, 10)])
	return missing, idx
	
def row_adjust(a, idx):
	rowdelta = [15 - sum(a[i]) for i in range(3)]
	cost = 0
	for i in range(3):	
		if rowdelta[i] != 0:
			for j in range(3):
				cost += replace_num(a, i, j, idx)
	return cost

def col_adjust(a, idx):
	coldelta = [15 - sum(a[i][j] for i in range(3)) for j in range(3)]
	cost = 0
	for j in range(3):	
		if coldelta[j] != 0:
			for i in range(3):
				cost += replace_num_col(a, i, j, idx)
	return cost

def check_magic(a):
	rows_ok = all([15 - sum(a[i])  == 0 for i in range(3)])
	cols_ok = all([15 - sum(a[i][j] for i in range(3))  == 0 for j in range(3)])
	return rows_ok and cols_ok

def swap_two_entries(a, p, q):
	xp, yp = p
	xq, yq = q
	tmpp, tmpq = a[xp][yp], a[xq][yq]
	a[xp][yp], a[xq][yq] = a[xq][yq], a[xp][yp]
	if check_magic(a):
		return abs(a[xp][yp] - tmpp) + abs(a[xq][yq] - tmpq)
	else:
		a[xp][yp], a[xq][yq] = tmpp, tmpq
		return 0
	
def col_intra_all_adjust(a, idx):
	pass

def col_intra_adjust(a, idx):
	cost = 0
	for row in range(3):
		rsum = sum(a[row])
		if rsum == 15:
			continue
		for col in range(3):
			if row == 0:
				mycost = swap_two_entries(a, (0, col), (1, col))
				if mycost != 0:
					return cost + mycost
				else:
					mycost = swap_two_entries(a, (0, col), (2, col))
					if mycost != 0:
						return cost + mycost
			elif row == 1:
				mycost = swap_two_entries(a, (1, col), (2, col))
				if mycost != 0:
					return cost + mycost
	return 0

def row_intra_all_adjust(a, idx):
	pass

def row_intra_adjust(a, idx):
	cost = 0
	for col in range(3):
		csum = sum([a[i][col] for i in range(3)])
		if csum == 15:
			continue
		for row in range(3):
			if col == 0:
				mycost = swap_two_entries(a, (row, 0), (row, 1))
				if mycost != 0:
					return cost + mycost
				else:
					mycost = swap_two_entries(a, (row, 0), (row, 2))
					if mycost != 0:
						return cost + mycost
			elif col == 1:
				mycost = swap_two_entries(a, (row, 1), (row, 2))
				if mycost != 0:
					return cost + mycost
	return 0

def magic0(a, cost = 0):
	missing, idx = miss(a)
	cost = 0
	while missing:
		newcost = deduplicate(a, idx)
		if newcost == 0:
			break
		cost += newcost
		missing, idx = miss(a)

	chk = check_magic(a)
	while not chk or missing:
		cost += row_adjust(a, idx)
		cost += col_adjust(a, idx)
		chk = check_magic(a)
		mycost = deduplicate(a, idx)
		if mycost == 0:
			break
		cost += mycost
		missing, idx = miss(a)
	if missing:
		cost += deduplicate_force(a, idx)

	rows_ok = all([15 - sum(a[i])  == 0 for i in range(3)])
	cols_ok = all([15 - sum(a[i][j] for i in range(3))  == 0 for j in range(3)])
	if cols_ok and rows_ok:
		return cost, a

	if rows_ok:
		cost += rows_intra_adjust(a, idx)
	else:
		cost += ducks_in_row(a, idx)
	if check_magic(a):
		return cost

	if cols_ok:
		cost += rows_intra_adjust(a, idx)
	else:
		cost += ducks_in_col(a, idx)
	if check_magic(a):
		return cost

	return -cost, a

def find_partials(a):
	row_partials = []
	for i in range(3):
		if sum(a[i]) == 15:
			partial = dict()
			for j in range(3):
				partial[a[i][j]] = (i, j)
			row_partials.append(partial)
		else:
			x = [(i, j) for j in range(3)]
			for j in range(3):
				y = a[i][:j] + a[i][j+1:]
				if sum(y) < 16 and 15 - sum(y) < 10 and y[0] != y[1]:
					z = x[:j] + x[j+1:]
					parts = dict()
					for p, q in z:
						parts[a[p][q]] = (p, q)
					row_partials.append(parts)
	return row_partials

def extend_cols(a, row, partial, cost, todo):
	if not extend_cols_inplace(a, row, partial, cost, todo):
		extend_cols_force(a, row, partial, cost, todo)

def extend_cols_force(a, row, partialx, cost, todo):
	for j in range(3):
		csum = sum(a[i][j] for i in range(3))
		if csum == 15:
			continue
		for num in range(1, 10):
			f = 15 - num - a[row][j]
			if num not in partialx and f > 0 and f < 10 and f not in partialx:
				addtl_cost = 0
				partial = partialx.copy()
				aprime = [[a[i][j] for j in range(3)] for i in range(3)]
				tmp = []
				tmp.append(num)
				tmp.append(f)
				x = 0
				for i in range(3):
					if i == row:
						continue
					aprime[i][j] = tmp[x]	
					partial[aprime[i][j]] = (i, j)
					addtl_cost += abs(aprime[i][j] - a[i][j])
					x += 1
				todo.append((aprime, partial, cost + addtl_cost))
	return

def extend_cols_inplace(a, row, partialx, cost, todo):
	for j in range(3):
		csum = sum(a[i][j] for i in range(3))
		if csum == 15:
			continue
		for i in range(3):
			if i == row:
				continue
			if a[i][j] not in partialx:
				filler = 15 -  a[i][j] - a[row][j]
				if filler > 0 and filler < 10 and filler not in partialx:
					partial = partialx.copy()
					partial[a[i][j]] = (i, j)
					y = [0 for z in range(3)]
					y[row] = -1
					y[i] = -1
					x = y.index(0)
					partial[filler] = (x, j)	
					addtl_cost = abs(a[x][j] - filler)
					aprime = [[a[p][q] for q in range(3)] for p in range(3)]
					aprime[x][j] = filler
					todo.append((aprime, partial, cost + addtl_cost))
					return True

def magic(a):
	results = []
	partials = find_partials(a)
	todo = [(a, partial, 0) for partial in partials]
	while todo:
		amat, partial, cost = todo.pop()
		if check_magic(amat):
			results.append((cost, amat))
			continue
		rowsum = dict()
		oldrow = None
		sum = 0
		blank = [0 for j in range(3)]
		for k, v in sorted(partial.items(), key = lambda x: x[1][0]):
			row, col = v
			if oldrow is None:
				oldrow = row
			elif oldrow != row:
				rowsum[oldrow] = sum
				sum = 0
				oldrow = row
				blank = [0 for j in range(3)]
			blank[col] = k
			sum += k
		rowsum[oldrow] = sum
		for row in sorted(rowsum.keys()):
			if rowsum[row] == 15: 
				extend_cols(amat, row, partial, cost, todo)
				continue
			if row == 0 or row == 1:
				continue
			filler = 15 - rowsum[row]
			if filler not in partial:
				col = blank.index(0)
				addtl_cost = abs(filler - amat[row][col])
				#newmat = amat[:][:]
				newmat = [[amat[i][j] for j in range(3)] for i in range(3)]
				newmat[row][col] = filler
				partial[filler] = (row, col)
				todo.append((newmat, partial, cost + addtl_cost))

	return results[0]

if __name__ == "__main__":
	#square = [[randrange(1, 10) for j in range(3)] for i in range(3)]
	#square = [[3, 6, 2], [5, 5, 6], [7, 2, 8]]
	#square = [[3, 9, 1], [5, 4, 6], [7, 2, 8]]
	#square = [[3, 9, 1], [5, 4, 6], [7, 2, 8]]
	#square = [[3, 6, 1], [4, 5, 6], [7, 2, 8]]
	#square = [[4, 8, 2], [4, 5, 7], [6, 1, 6]]
	#square = [[2, 2, 7], [8, 6, 4], [1, 2, 9]]
	square = [[3, 6, 2], [5, 5, 6], [7, 2, 8]]
	#for i in range(3):
		#print tuple(square[i])
	cost, mat = magic(square)
	print cost
	for i in range(3):
		print tuple(mat[i])
