#!/usr/bin/env python

def webtoarr(fd):
	arr = []
	for line in fd.readlines():
		line.strip()
		arr += line.split()
	return arr
def shortest(arr, stext):
	from collections import defaultdict
	idx = defaultdict(int)
	shortest, pair = 9999, (-1, -1)
	for off,s in enumerate(arr):
		if s in stext:
			idx[s] = off
			iarr = [idx[stext[i]] for i in range(len(stext))]	
			print iarr
			if shortest != 9999 or all([iarr[i] != 0 for i in range(len(iarr))]):
				if (max(iarr) - min(iarr)) < shortest:
					shortest = max(iarr) - min(iarr)
					pair = iarr
	return pair
if __name__ == "__main__":
	import sys
	arr = webtoarr(open(sys.argv[1]))
	print shortest(arr, sys.argv[2:])
