#!/usr/bin/env python

def intersect0(a, b):
	i = j = 0
	tmp = None
	out = []
	while i < len(a) and j < len(b):
		if a[i] == tmp:
			i += 1
		if b[j] == tmp:
			j += 1

		if i >= len(a) or j >= len(b):
			return out

		if a[i] == b[j]:
			tmp = a[i]
			out.append(tmp)
			i += 1
			j += 1
		elif a[i] > b[j]:
				j += 1
		else:
			i += 1	
	return out

def intersect(a, b):
	i = j = 0
	out = []
	while i < len(a) and j < len(b):
		if a[i] == b[j] and (i == 0 or a[i] != a[i-1]):
			out.append(a[i])
			i += 1
			j += 1
		elif a[i] > b[j]:
				j += 1
		else:
			i += 1	
	return out
if __name__ == "__main__":
	from random import randrange 
	import sys
	m, n = map(int, sys.argv[1:])
	a = [randrange(m * 2) for i in range(m)]
	b = [randrange(n * 2) for i in range(n)]
	a.sort(), b.sort()
	#a = [0, 0, 2, 3, 3, 12, 17, 19, 20, 21, 21]
	#b = [0, 1, 6, 11, 12, 14, 15, 16, 17]
	print a, b
	out = intersect(a, b)
	print out
