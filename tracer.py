#!/usr/bin/env python
"""
	A class to trace calls to a function.
"""

class tracer:
	def __init__(self, func):
		self.calls = 0
		self.func = func
	def __call__(self, *args):
		self.calls += 1
		return self.func(*args)
