#!/usr/bin/env python
import random

def split_list(l, k):
	pivot = random.choice(l)
	lower, middle, higher = [[] for i in range(3)]
	for x in l:
		if x > pivot:
			higher.append(x)
		elif x == pivot:
			middle.append(pivot)
		else:
			lower.append(x)
	if len(l) != len(middle):
		return lower + middle, higher
	else:
		return middle[:k], []

def find_highest(l, k):
	low, high = split_list(l, k)
	while len(low) != k:
		if len(low) < k:
			k = k - len(low)
			low = high
		low, high = split_list(low, k)
	return low[-1]

if __name__ == "__main__":
	import sys
	k = int(sys.argv[1])
	l = [random.randrange(k * k) for i in range(k * random.randrange(1, k * 2) + 1)]
	lsorted = sorted(l)
	print "unsorted l = ", l
	pick = random.randrange(1, k)
	print "sorted l = ", lsorted, "k_highest = ", lsorted[pick - 1]
	k_highest = find_highest(l, pick)
	assert(lsorted[pick - 1] == k_highest)
	print "pick = ", pick, "kth highest = ", k_highest
