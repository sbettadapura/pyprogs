#!/usr/bin/env python

def union_intervals(intervals):
	tmp = intervals[0]
	out = []
	for invl in intervals[1:]:
		if tmp[1] < invl[0]:
			out.append(tmp)
			tmp = invl
		else:
			tmp[0] = min(tmp[0], invl[0])	
			tmp[1] = max(tmp[1], invl[1])	
	out.append(tmp)
	return out

if __name__ == "__main__":
	intervals = [[0, 3], [5, 7], [9, 11], [12, 14], [1, 1], [3, 4], [7, 8], [12, 16], [2, 4], [8, 11], [13, 15], [16, 17]]
	intervals.sort()
	print intervals
	print union_intervals(intervals)
