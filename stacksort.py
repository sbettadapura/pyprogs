#!/usr/bin/env python

def stacksort(src, minbot = None, dest = []):
	if len(src) == 0:
		return dest
	bot = src.pop()
	while src:
		x = src.pop()
		if x < bot:
			dest.append(bot)
			bot = x
		else:
			dest.append(x)
	if minbot is None:
		minbot = bot - 1
	while dest and dest[-1] > minbot:
		src.append(dest.pop())
	dest.append(bot)	
	stacksort(src, bot, dest)
	return dest

if __name__ == "__main__":
	from random import randrange
	print stacksort([randrange(1000) for i in range(randrange(25))])

