#!/usr/bin/env python
"""
	rotate a NXN matrix in-place. Row 0 at top, column 0 at left. Do this in 
	"layers". The 0th layer consists of the outside edges. The 1st layer
	consists of locations in the next inside square and so on. The number of
	squares in layer l is (n - 2 * l). When l = n/2, no. is size, so stop 
	before that.
	Pick a location in layer l which is
	i columns from the left. This is L[l][l+i]. Map this location clockwise
	to all other sides. Then keep swapping the values around.
	Map all location except the one is the corner. Hence (n - l * 2 - 1)
"""
def rotate(L):
	n = len(L)
	for l in range(n / 2):
		for i in range(n - 1 - l * 2):
			save = L[l][l+i]
			L[l][l+i] = L[n - l - i - 1][l]
			L[n - l - i - 1][l] = L[n - l -1][n - l - i - 1]
			L[n - l -1][n - l - i - 1] = L[l + i][n - l - 1]
			L[l + i][n - l - 1] = save

if __name__ == "__main__":
	import sys
	from random import randrange
	try:
		num = int(sys.argv[1])
	except:
		print "usage: %s [size of matrix to rotate]" % sys.argv[0]
		exit(1)
	L = [[0] * num for i in range(num)]
	for i in range(num):
		for j in range(num):
			L[i][j] = randrange(num * 10)
	for i in range(num):
		print L[i]
	rotate(L)
	print "after rotation"
	for i in range(num):
		print L[i]
