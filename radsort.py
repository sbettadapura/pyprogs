#!/usr/bin/env python
"""
	Radix sort an array. Partition the array into two sections: the one to the
	left consists of numbers without a bit being set, while the ones on the
	right have the bit set. Call radsort() recursively on the two sections
	for the next lower bit. An additional detail is that if numbers have been
	swapped in radsort() you return this indication, so the caller knows to 
	call back the function with the same input for the next lower bit.
"""
def partition(a, left, right, bitpos):
	swapped = 0
	if right == 1 or a == []:
		return 0, 0

	while left < right:
		if (a[left] & (1 << bitpos)) >> bitpos == 1:
			right -= 1
			a[left], a[right] = a[right], a[left]	
			swapped = 1
		else:
			left += 1

	return swapped, left

def radsort(a, left, right, bitpos = 31):
	swapped, mid = partition(a, left, right, bitpos)
	if bitpos == 0:
		return
	if swapped:
		radsort(a, left, mid, bitpos - 1)
		radsort(a, mid, right, bitpos - 1)
	else:
		radsort(a, left, right, bitpos - 1)

if __name__ == "__main__":
	import sys
	from random import randrange
	num = int(sys.argv[1])
	a = [randrange(num * num) for i in range(num)]
	print a
	radsort(a, 0, len(a))
	print a
