#!/usr/bin/env python
from collections import defaultdict
from bisect import bisect
def lnds_brute(l):
	n = len(l)
	best = []
	for i in range(n):
		tmp = [l[i]]
		for j in range(i + 1, n):
			if l[j] >= tmp[-1]:
				tmp.append(l[j])
		best = max(best, tmp, key = len)	
	return best
def lnds_b(l, memo):
	n = len(l)
	if n == 1:
		return l
	if tuple(l) in memo:
		return memo[tuple(l)]

	best = []
	for i in range(n - 1):
		prev = lnds_b(l[i:-1], memo)
		if prev[-1] <= l[-1]:
				tmp = prev[:] + [l[-1]]
		else:
				tmp =  prev
		best = max(best, tmp, key = len)	
	memo[tuple(l)] = best
	return best	
def lnds_f(l):
	n = len(l)
	d = defaultdict(list)
	d[1] = l[:1]
	for i in range(1, n):
		dk = sorted(d.keys())
		for j in range(len(dk) - 1, -1, -1):
			seq = d[dk[j]]
			if seq == []: continue
			if seq[-1] < l[i]:
				xseq = seq[:]
				xseq.append(l[i])
				os = d[len(xseq)]
				if os != []:
					if os[-1] > xseq[-1]:
						d[len(xseq)] = xseq
				else:
					d[len(xseq)] = xseq
				break
		else:
			os = d[1]
			if os:
				if os[-1] > l[i]:
					d[1] = l[i:i+1]
			else:
				d[1] = l[i:i+1]

	k = max(sorted(d.keys()))
	return d[k]
def lnds_f_l(l):
	n = len(l)
	lseq = [(1, l[:1])]
	for i in range(1, n):
		for xl, seq in lseq:
			if l[i] >= seq[-1]:
				idx = bisect(lseq, ((xl + 1, (0, 0))))
				if idx != 0:
					tseq = lseq[idx - 1]
					if tseq[0] == xl + 1 and tseq[-1] >= l[i]:
						lseq.remove(tseq)
						
				xseq = seq[:] + [l[i]]
				lseq.append((len(xseq), xseq))
				lseq.sort(reverse = True, key = lambda x: x[0])
				break
		else:
			lseq.append((1, l[i:i+1]))
			lseq.sort(reverse = True, key = lambda x: x[0])
	return lseq[0][-1]

def lnds_f_l_1(l):
	n = len(l)
	dlseq = defaultdict(list)
	dlseq[1] = l[:1]
	lseq = [(1, l[:1])]
	for i in range(1, n):
		for xl, seq in lseq:
			if l[i] >= seq[-1]:
				if xl + 1 in dlseq:
					tseq = dlseq[xl + 1]
					if tseq[0] == xl + 1 and tseq[-1] >= l[i]:
						del dlseq[xl + 1]
						lseq.remove((len(tseq), tseq))
					
				xseq = seq[:] + [l[i]]
				dlseq[xl + 1] = xseq
				lseq.append((len(xseq), xseq))
				lseq.sort(reverse = True, key = lambda x: x[0])
				break
		else:
			dlseq[1] = l[i:i+1]
			lseq.append((1, l[i:i+1]))
			lseq.sort(reverse = True, key = lambda x: x[0])
	return lseq[0][-1]

def lnds_f_l_2(l):
	n = len(l)
	dlseq = defaultdict(list)
	dlseq[1] = l[:1]
	for i in range(1, n):
		dlk = sorted(dlseq.keys(), reverse = True)
		for xl in dlk:
			seq = dlseq[xl]
			if l[i] >= seq[-1]:
				dlseq[xl + 1] = seq[:] + [l[i]]
				break
		else:
			dlseq[1] = l[i:i+1]
	return dlseq[max(dlseq.keys())]

def lnds_f_l_3(l):
	n = len(l)
	dlseq = defaultdict(list)
	dlseq[1] = l[:1]
	for i in range(1, n):
		dlk = dlseq.keys()
		for j in range(len(dlk)-1, -1, -1):
			xl = dlk[j]
			seq = dlseq[xl]
			if l[i] >= seq[-1]:
				dlseq[xl + 1] = seq[:] + [l[i]]
				break
		else:
			dlseq[1] = l[i:i+1]
	return dlseq[max(dlseq.keys())]
def lnds_f_l_4(l):
	n = len(l)
	dlseq = defaultdict(list)
	dlseq[1] = l[:1]
	for i in range(1, n):
		for xl in reversed(dlseq.keys()):
			seq = dlseq[xl]
			if l[i] >= seq[-1]:
				dlseq[xl + 1] = seq[:] + [l[i]]
				break
		else:
			dlseq[1] = l[i:i+1]
	return dlseq[max(dlseq.keys())]

if __name__ == "__main__":
	import sys
	try:
		num = int(sys.argv[1])
	except:
		print "usage %s [size of array]" % sys.argv[0]
		exit(1)
	from random import randrange
	from collections import defaultdict
	memo = defaultdict(list)
	l = [randrange(num * num) for i in range(num)]
	#l = [3, 11, 9, 48, 16, 42, 46]
	#l = [4, 27, 30, 6, 35, 23, 24]
	#l = [30, 128, 78, 7, 85, 140, 82, 201, 175, 190, 270, 201, 136, 140, 175, 71, 125]
	#l = [240, 143, 286, 40, 16, 177, 159, 232, 181, 84, 101, 66, 226, 61, 20, 152, 240]
	#l = [236, 27, 171, 178, 14, 6, 248, 10, 25, 200, 65, 253, 68, 148, 64, 210, 119]
	#l = [10, 20, 9, 18, 19]
	print l
	from collections import defaultdict
	memo = defaultdict(list)
	print "brut   = ", lnds_brute(l)
	print "algo b = ", lnds_b(l, memo)
	print "algo f = ", lnds_f(l)
	print "alg fl = ", lnds_f_l(l)
	print "al fl2 = ", lnds_f_l_2(l)
	print "al fl3 = ", lnds_f_l_3(l)
	print "al fl4 = ", lnds_f_l_4(l)
	print "al fl1 = ", lnds_f_l_1(l)
