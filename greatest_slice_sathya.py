#!/usr/bin/env python
"""
	Given an array find the slice with greatest sum.
	[12, 2, 15, -4, 18] [15, -4], 18] is the answer.
	If all numbers are negative the empty [] is the solution.

	This is my version of Hetland's algorithm which is 
	in geatest_slice_hetland.py. I believe there's a bug in
	his algorithm that my version fixes.
"""
from greatest_slice_alt import file_to_arr

A = [54, 32, -23, 18, -8]
#A = [-2, 7, 1, 5, 19, -2, 21, 42]
#A = [-2, 7, 1, 5, 19, -2, -21, 42]

def gr_slice(l):
	n = len(l)
	best = l[0]
	for size in range(1, n+1):
		cur = sum(l[:size])
		best = max(best, cur)
		for i in range(n-size):
			cur += l[i+size] - l[i]
			best = max(best, cur)
	return best

if __name__ == "__main__":
	import sys
	#print gr_slice(file_to_arr(open(sys.argv[1])))
	print gr_slice(A)
