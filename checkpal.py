#!/usr/bin/env python

import sys
str = sys.argv[1]

from collections import defaultdict
d = defaultdict(int)
for s in str:
	d[s] += 1
if sum([d[k] & 1 for k in d.keys()]) > 1:
	print "%s not a permutation of palindrome" % str
else:
	print "yes, %s is a permutation of palindrome" % str
