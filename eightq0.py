#!/usr/bin/env python
def allowed_rows(col, queens):
	forbidden = set([q[0] for q in queens])
	for r, c in queens:
		for i in (1, -1):
			xr = r + i * (col - c) 
			if xr < 8 and xr >= 0:
				forbidden.add(xr)
	return set(range(8)) - forbidden	
	
def eightq(res, queens = [], st_col = 0):
	if len(queens) == 8:
		res.append(queens[:])
		return

	for col in range(st_col, 8):
		rows = allowed_rows(col, queens)
		for row in rows:
			queens.append((row, col))
			eightq(res, queens, col + 1)
			queens.pop()

if __name__ == "__main__":
	res = []
	eightq(res)
	for x in res:
		print x
