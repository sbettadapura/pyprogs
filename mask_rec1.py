#!/usr/bin/env python
"""
	Given a width, produce all bit combinations.
	Given 3, 000, 001, 010, 011, 100, 101, 110, 111.
"""

def mask(cnt):
	if cnt == 0:
		return ['']
	x = []
	for i in mask(cnt - 1):
		x.append(i + '0')
		x.append(i + '1')
	return x
if __name__ == "__main__":
	import sys
	try:
		print mask(int(sys.argv[1]))
	except:
		print "usage %s [width of mask]" % sys.argv[0]
		exit(1)
