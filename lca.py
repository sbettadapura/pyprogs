#!/usr/bin/env python
class Tree:
	def __init__(self, val, left = None, right = None):
		self.parent = None
		self.val = val
		self.left = left
		self.right = right
		if left:
			left.parent = self
		if right:
			right.parent = self

def exists(tree, val):
	if tree is None:
		return False
	elif tree.val == val:
		return tree
	else:
		x = exists(tree.left, val)
		if x:
			return x
		else:
			return exists(tree.right, val)

def lca(p, q):
	x = p
	while x and not exists(x, q.val):
		x = x.parent
	return x

if __name__ == "__main__":
	t1 = Tree(80, Tree(90), Tree(100))
	t2 = Tree(20, Tree(70), t1)
	t3 = Tree(50, Tree(40), Tree(60))
	t4 = Tree(10, t3, t2)
	tx = lca(exists(t4, 50), exists(t4, 100))
	if tx:
		print tx.val
	else:
		print "root"
