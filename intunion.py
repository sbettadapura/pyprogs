#!/usr/bin/env python

def int_union(intvls):
	res = []
	b, e = intvls[0]
	for xb, xe in intvls[1:]:
		if xb > e:
			res.append((b, e))
			b, e = xb, xe
		elif xb <= e:
			if xe > e:
				e = xe
	res.append((b, e))
	return res

if __name__ == "__main__":
	intvls = [(0, 8), (3, 6), (4, 10), (12, 18), (14, 20), (16, 22)]
	a = [(0, 3), (1, 1), (2, 4), (3, 4), (5, 7), (7, 8), (8, 11), (9, 11), (12, 14), (12, 16), (13, 15), (16, 17)]
	res = int_union(intvls)
	print res
	res = int_union(a)
	print res
