#!/usr/bin/env python

def bsgrk(a, n, k):
	l, h = 0, n -1
	res = -1
	while l <= h:
		m = (l + h) / 2
		if a[m] > k:
			h = m -1
		else:
			res =  l = m + 1
	return res

def bsgrk_1(a, n, k):
	l, h = 0, n -1
	res = -1
	while l <= h:
		m = (l + h) / 2
		if a[m] > k:
			res = m
			h = m -1
		else:
			l = m + 1
	return res
if __name__ == "__main__":
	from random import randrange
	import sys
	n = int(sys.argv[1])
	a = sorted([randrange(n * n) for i in range(n)])
	x = randrange(min(a), max(a))
	print a, x
	y =  bsgrk(a, n, x)
	if y < n:
		print a[y]
	else:
		print a[n - 1] + 1
