def cont_1s(grid, x, y, m, n):
	if grid[x][y] == '0':
		return 0

	grid[x][y] = '0'
	cont1s = 1

	for dx, dy in ((+1, 0), (-1, 0), (0, +1), (0, -1)):
		x1 = x + dx
		y1 = y + dy
		if x1 >=0 and x1 < n and y1 >=0 and y1 < m:
			cont1s += cont_1s(grid, x1, y1, m, n)
	return cont1s

class Solution(object):
	def numIslands(self, grid):
		res = 0
		n = len(grid)
		m = len(grid[0])
		for i in range(n):
			grid[i] = list(grid[i])
		for i in range(n):
			for j in range(m):
				if cont_1s(grid, i, j, m, n) > 0:
					res += 1
		return res

if __name__ == "__main__":
	s = Solution()
	grid = ["11110", "11010", "11000", "00000"]
	print s.numIslands(grid)
	grid = ["11000", "11000", "00100", "00011"]
	print s.numIslands(grid)
	grid = ["1", "1"]
	print s.numIslands(grid)
