#!/usr/bin/env python
"""
	Given a dictionary of adjacency sets, a starting and ending node,
	find the shortest distance between the two points.
"""
def shortest(g, s, f, P):
		if s not in P:
			P[s] = None

		if f in g[s]:
			P[f] = s	
			return g[s][f]
		else:
			d = 999999
			next = None
			for v in g[s]:
				#P[v] = s
				dist =  g[s][v] + shortest(g, v, f, P)
				if dist < d:
					d = dist
					next = v	
			P[next] = s	
			return d

if __name__ == "__main__":
	import sys
	g = {}
	g = {
		0: {1:8, 2:19, 3:12},
		1: {3:7, 4:5},
		2: {5:17},
		3: {4:12, 5:12},
		4: {5:11},
		5: {}
		}

	print "Adjacency set:"
	for k in g.keys():
		print k, ": ", g[k]

	P = dict()
	s, f = map(int, sys.argv[1:])
	print shortest(g, s, f, P)
	print P
