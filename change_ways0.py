#!/usr/bin/env python

def chway(amt, denom):
	ways = 0
	for i, c in enumerate(denom):
		if amt < c:
			continue
		elif amt == c:
			ways += 1
		else:
			ways += chway(amt - c, denom[i:])
	return ways

if __name__ == "__main__":
	import sys
	try:
		amt = int(sys.argv[1])
	except:
		print "usage %s {amt to be changed]" % sys.argv[0]
		exit(1)
	print amt, "ways in which it can be changed = ", chway(amt, [25, 10, 5, 1])
