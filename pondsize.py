#!/usr/bin/env python
wheel = ((-1,0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1))
def pondsize(a, i, j, n):
	if a[i][j] == 1:
		return 0
	a[i][j] = 1
	psize = 1
	for ix, iy in wheel:
		x = i + ix
		y = j + iy
		if x >= 0 and x < n and y >= 0 and y < n:	
			psize += pondsize(a, x, y, n)
	return psize

if __name__ == "__main__":
	from random import randrange
	import sys
	try:
		num = int(sys.argv[1])
	except:
		print "usage %s [size of land]" % sys.argv[0]
		exit(1)
	larr = [[0] * num for i in range(num)]
	for i in range(num):
		for j in range(num):
			larr[i][j] = randrange(2)
	for i in range(num):
		print larr[i]
	ponds = []
	for i in range(num):
		for j in range(num):
			x = pondsize(larr, i, j, num)
			if x > 0:
				ponds.append(x)
	print ponds
