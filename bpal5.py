#!/bin/python

import os
import sys
#
# Complete the buildPalindrome function below.
#
def ispal(arr):
	for i in range(len(arr) / 2):
		if arr[i] != arr[len(arr) - 1 - i]:
			return False
	else:
		return True
	
def maxpal(arr):
	for i in range(len(arr)):
		if ispal(arr[i:]):
			return arr[i:]	

def checkpal(asub, b):
	if b == '':
		if len(asub) == 1:
			return asub
		else:
			return checkpal(asub[0], asub[1:])
	
	asubl = list(asub)
	arev = ''.join([y for y in reversed(asubl)])
	res = ''
	if arev in b:
		ind =  b.index(arev)
		if ind > 0:
			res = asub + maxpal(b[:ind]) + arev
		else:
			res = asub + arev
	else:
		x = arev[1:]
		while x:
			y = asub + x
			if x in b and ispal(y):
				return y
			x = x[1:]
	return res

def buildPalindrome(a, b):
	max_res = ''
	for i in xrange(len(a), 0, -1):
		for j in xrange(len(a)):
			if j + i > len(a):
				continue
			res = checkpal(a[j:j+i], b)
			if len(res) > len(max_res):
				max_res = res
			elif len(res) == len(max_res):
				if res < max_res:
					max_res = res
	if max_res == '':
		max_res = str(-1)
	return max_res

if __name__ == '__main__':
	f = open(sys.argv[1])
	#fptr = open(os.environ['OUTPUT_PATH'], 'w')
	#t = int(raw_input())
	t = int(f.readline().rstrip())
	for t_itr in xrange(t):
		#a = raw_input()
		a = f.readline().rstrip()
		#b = raw_input()
		b = f.readline().rstrip()
		#fptr.write(result + '\n')
		result = buildPalindrome(a, b)
		print result
	#fptr.close()
