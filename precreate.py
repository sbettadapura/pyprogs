#!/usr/bin/env python
LEFT, RIGHT = range(2)
class Tree:
	def __init__(self, val, left = None, right = None):
		self.val = val
		self.left = left
		self.right = right	

def preorder(tree):
	if tree:
		print tree.val,
		if tree.left:
			preorder(tree.left)
		else:
			print 'x',
		if tree.right:
			preorder(tree.right)
		else:
			print 'x',

def	precreate_1(preord):
	rtst = []
	n = len(preord)
	rtst = [(LEFT, preord[0])]
	for i in range(1, n):
		if preord[i] != 'x':
			rtst.append((LEFT, preord[i]))
		else:
			expecting, obj = rtst.pop()
			if expecting == LEFT:
				rtst.append((RIGHT, Tree(obj, None)))
			else:
				obj.right = None
				while rtst:
					expecting, parent = rtst.pop()
					if expecting == LEFT:	
						rtst.append((RIGHT, Tree(parent, obj)))
						break
					else:
						parent.right = obj
						obj = parent	
	return parent

def	precreate_2(preord):
	rtst = []
	n = len(preord)
	rtst = [(LEFT, preord[0])]
	for i in range(1, n):
		if preord[i] != 'x':
			rtst.append((LEFT, preord[i]))
		else:
			child, parent = None, None
			while rtst:
				expecting, obj = rtst.pop()
				if child:
					parent = obj
				if expecting == LEFT:
					if parent:
						rtst.append((RIGHT, Tree(parent, child)))
					else:
						rtst.append((RIGHT, Tree(child, None)))
					break
				else:
					if parent:
						parent.right = child
						child = parent
	return parent

def	precreate_3(preord):
	n = len(preord)
	roots = [(LEFT, preord[0])]
	for i in range(1, n):
		if preord[i] != 'x':
			roots.append((LEFT, preord[i]))
		else:
			child = None
			while roots:
				expecting, parent = roots.pop()
				if expecting == LEFT:
					roots.append((RIGHT, Tree(parent, child)))
					break
				else:
					parent.right = child
					child = parent
	return parent
from collections import deque

def by_level(tree):
	oq = deque([tree])
	while oq:
		curr_level = deque(oq)
		oq = []
		while curr_level:
			root = curr_level.popleft()	
			print root.val,	
			if  root.left:
				oq.append(root.left)
			if  root.right:
				oq.append(root.right)
		print "\n",

if __name__ == "__main__":
	fullt = precreate_3("hbfxxeaxxxcxdxgixxx")
	print "preorder = ",
	preorder(fullt)
	print "\nby level = "
	by_level(fullt)
