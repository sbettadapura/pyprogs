#!/usr/bin/env python
def arr_root(a):
	left, right = 0, len(a) - 1
	while left < right:
		mid = (left + right) / 2
		#print left, right, mid, a[mid]
		if a[mid] < a[right]:
			right = mid
		else:
			left = mid + 1
	return left

def cyclic_search(a, n, k):
	left, right = 0, n - 1
	res = -1
	while left <= right:
		mid = (left + right) / 2	
		print left, right, mid, a[mid]
		if a[mid] == k:
			return mid
		elif a[mid] < k:
			if a[right] >= k:
				left = mid + 1
			else:
				right = mid - 1
		else:
			if a[left] <= k:
				right = mid - 1
			else:
				left = mid + 1
	return res

def cyclic_search_1(a, n, k):
	left, right = 0, n - 1
	res = -1
	while left <= right:
		mid = (left + right) / 2	
		print left, right, mid, a[mid]
		if a[mid] == k:
			return mid
		elif (
			(a[mid] < k and k <= a[right]) or 
			(a[right] < a[mid] and a[mid] <= k) or
			(k <= a[right] and a[right] < a[mid])
			):
				left = mid + 1	
		elif (
			(a[left] <= k and k < a[mid]) or
			(a[mid] <= a[left] and a[left] <= k) or 
			(k < a[mid] and a[mid] <= a[left])
			):
				right = mid - 1
	return res
if __name__ == "__main__":
	from random import randrange
	import sys
	n, r = map(int, sys.argv[1:])
	a = sorted([randrange(n * n) for i in range(n)])
	print a
	b = a[n-r:]
	print b
	a = b + a[:n-r]
	x = a[randrange(n)]	
	#a = [148, 163, 176, 211, 230, 235, 241, 247, 248, 3, 4, 12, 38, 119, 121, 124] 
	#x = 248
	print a, x
	print "cyclic found: ", cyclic_search_1(a, n, x)
	print "arr_root ", arr_root(a)
