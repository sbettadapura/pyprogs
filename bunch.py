#!/usr/bin/env python

def bunch(a):
	from collections import defaultdict
	num_to_count = defaultdict(int)
	for x in a:
		num_to_count[x] += 1
	for i in range(len(num_to_count.keys())):
		num = num_to_count.keys()[i]
		count = num_to_count.values()[i]
		for j in range(sum(num_to_count.values()[:i]), sum(num_to_count.values()[:i]) + count):
			f = a[j:].index(num)
			a[j], a[j + f] = a[j + f], a[j]	
	return a

if __name__ == "__main__":
	"""
	a = [14, 12, 11, 15, 14, 11, 13, 11, 12, 15, 13, 14, 15]
	print a
	b = bunch(a)
	print b
	"""
	import sys
	from random import randrange
	from random import shuffle
	try:
		start_age, rang, how_many = map(int, sys.argv[1:])
	except:
		print "usage: %s, <starting age> <range> <how many>" % sys.argv[0]
		exit(1)
	end_age = start_age + randrange(rang)
	#print "start_age = ", start_age, "end_age = ", end_age
	a = []
	for i in range(start_age, end_age):
		how = randrange(how_many)
		#print "how = ", how
		a += [i for j in range(how)]	
	shuffle(a)
	print a 
	b = bunch(a)
	print b
