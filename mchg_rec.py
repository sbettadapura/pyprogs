#!/usr/bin/env python
"""
	Given a set of coins like [25, 10, 5, 1] list all the ways
	in which you can make change for a given amount.
"""
from tracer import tracer

@tracer
def make_change(amt, coins, xdict = dict()):
	xlist = []
	for i, c in enumerate(coins):
		if amt < c: 
			continue
		elif amt == c:
			xlist.append([c])
		else:	
			smaller = amt - c
			lists = xdict.get((smaller, tuple(coins[i:])))
			if lists is None:
				lists = make_change(smaller, coins[i:], xdict)
			xlist += [x + [c] for x in [x[:] for x in lists]]

	xdict[(amt, tuple(coins))] = xlist
	return xlist

if __name__ == "__main__":
	import sys
	coins = [25, 10, 5, 1]
	try:
		amt = int(sys.argv[1])
	except:
		print "usage: %s [amount you need change for]" % sys.argv[0]
		exit(1)
	lists = make_change(amt, coins)

	for l in lists:
		l.sort(reverse=True)
	lists.sort(reverse=True)

	print "Ways of changing %d using " % amt, coins
	for l in lists:
		print tuple(l)
	print "calls to make_change() = ", make_change.calls
