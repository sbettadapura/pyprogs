#!/usr/bin/env python
from random import randrange

if __name__ == "__main__":
	import sys
	from collections import defaultdict
	d = defaultdict(int)
	times = int(sys.argv[1])
	for i in range(times):
		rand5 = randrange(5)
		d[rand5] += 1	
	print d
		
