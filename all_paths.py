#!/usr/bin/env python
"""
	Given a dictionary of adjacency sets, a starting and ending node,
	find all paths that lead from the given starting node to the end point.
"""
def get_path(x, P):
	path = [x]
	while P[x] is not None:
		path.append(P[x])
		x = P[x]
	path.reverse()
	return path

def all_paths(g, s, f, res = [], P = dict()):
	if not s in P:
		P[s] = None
	for v in g[s]:
		P[v] = s
		if v != f:
			res = all_paths(g, v, f, res, P)
		else:
			path = get_path(f, P)
			res.append(path)
	return res

if __name__ == "__main__":
	g = {
		0: {1, 2, 3},
		1: {3, 4},
		2: {5},
		3: {4, 5},
		4: {5},
		5: set()
		}

	print "Adjacency set:"
	for k in g.keys():
		print k, ": ", g[k]

	res = all_paths(g, 0, 5)
	for i in res:
		print i
