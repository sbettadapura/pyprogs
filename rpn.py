#!/usr/bin/env python

def rpn(exp):
	opst = []
	tokens = list(reversed(exp.split(',')))
	while tokens:
		token = tokens.pop().strip()
		if token in ("+-*/%"):
			y = opst.pop()
			x = opst.pop()
			opst.append(eval("x" + token + "y"))
		else:
			opst.append(int(token))
	return opst.pop()
if __name__ == "__main__":
	import sys
	print rpn(sys.argv[1])
