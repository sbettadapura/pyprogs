#!/usr/bin/env python
class Tree:
	def __init__(self, val, left = None, right = None):
		self.parent = None
		self.val = val
		self.left = left
		self.right = right
		if left:
			left.parent = self
		if right:
			right.parent = self

def checksum(tree, csum):
	if tree is None:
		return False
	elif tree.val == csum:
		return True
	else:
		return checksum(tree.left, csum - tree.val) or checksum(tree.right, csum - tree.val)	

if __name__ == "__main__":
	t1 = Tree(80, Tree(90), Tree(100))
	t2 = Tree(20, Tree(70), t1)
	t3 = Tree(50, Tree(40), Tree(60))
	t4 = Tree(10, t3, t2)
	print checksum(t4, 186)
