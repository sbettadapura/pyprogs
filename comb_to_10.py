#!/usr/bin/env python
def comb_to_win(pat, total):
	outstr = ""
	i = 0
	j = 0
	while i < len(pat) and  j < len(total):
		if pat[i] == total[j]:
			i += 1
			outstr += '1'
		else:
			outstr += '0'	
		j += 1
	return outstr	

def comb_to_win_2(pat, total):
	outstr = ""
	while pat:
		if pat[0] == total[0]:
			outstr += '1'
			pat = pat[1:]
		else:
			outstr += '0'
		total = total[1:]

	return outstr	

import sys
try:
	num = int(sys.argv[1])
except:
	print "usage: %s <number> <string>" % sys.argv[0]
	exit(1)

pat = sys.argv[2]
total = "abcdefghijklmnopqrstuvxyz"[:num]
print total, pat
print comb_to_win(pat, total)
print comb_to_win_2(pat, total)
