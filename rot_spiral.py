#!/usr/bin/env/python

def rot(a):
	n = len(a)
	for j in range(n):
		print a[j]
	for m in range(n/2):
		for i in range(0, n - 2 * m - 1):
			tmp = a[m][m + i]
			a[m][m + i] = a[m + i][n - m - 1]
			a[m + i][n - m - 1] = a[n - m - 1][n - m -1 - i]
			a[n - m - 1][n -m -1 - i] = a[n -m -1 -i][m]
			a[n -m -1 -i][m] = tmp
	print "after rot"
	for j in range(n):
		print a[j]
if __name__ == "__main__":
	import sys
	n = int(sys.argv[1])
	a = []
	for i in range(n):
		a.append([i * n + x for x in range(n)])
	rot(a)
