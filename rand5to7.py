#!/usr/bin/env python
from random import randrange

def rand5to7(num):
	sum = 0
	for i in range(num):
		#sum += randrange(5) + 5 * i
		sum += randrange(5)
	return sum % 7	

if __name__ == "__main__":
	import sys
	from collections import defaultdict
	d = defaultdict(int)
	num = int(sys.argv[1])
	times = int(sys.argv[2])
	for i in range(times):
		rand7 = rand5to7(num)
		d[rand7] += 1	
	print d
		
