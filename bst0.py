#!/usr/bin/env python
class Tree:
	def __init__(self, val, left = None, right = None):
		self.root = val
		self.left = left
		self.right = right
	def add(self, val):
		if val > self.root:
			self.right = Tree(self.right, val)
		else:
			self.left = Tree(self.left, val)
	def in_order(self, arr):
		if self.left is not None:
			self.left.in_order(arr)
		arr.append(self.root)
		if self.right is not None:
			self.right.in_order(arr)

"""
def find_highest(root, k):
	hi_arr = []
	root.in_order(hi_arr)
	return hi_arr[-k:]
"""
def walk_down(root, P):
	if root.right is None:
		if root.left is None:
			return root
		else:
			P[root.left] = root
			return walk_down(root.left, P)
	else:
		P[root.right] = root
		return walk_down(root.right, P)

def walk_up(root, P, hi_arr, k):
	if len(hi_arr) == k:
		return
	hi_arr.append(root.root)
	if root.left is not None:
		left_arr = find_highest(root.left, k - len(hi_arr))
		hi_arr.extend(left_arr)
		if len(hi_arr) >= k:
			return
	if P[root] is None:
		return
	walk_up(P[root], P, hi_arr, k)

def find_highest(root, k):
	P = {root:None}
	last = walk_down(root, P)
	hi_arr = []
	walk_up(last, P, hi_arr, k)
	return hi_arr

if __name__ == "__main__":
	t1 = Tree(5)
	t2 = Tree(20)
	t3 = Tree(10, t1, t2)
	t4 = Tree(40)
	t5 = Tree(60)
	t6 = Tree(50, t4, t5)
	t7 = Tree(30, t3, t6)
	print find_highest(t7, 5)
