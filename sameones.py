#!/usr/bin/env python

def num_ones(num):
	n1 = 0
	while num:
		n1 += num & 1
		num >>= 1
	return n1
def next_higher_one(num):
	n1 = num_ones(num)
	num += 1
	while num_ones(num) != n1:
		num += 1
	return num

def next_lower_one(num):
	n1 = num_ones(num)
	num -= 1
	while num_ones(num) != n1:
		num -= 1
	return num

def next_higher_one_opt(num):
	x, zeros, ones = num, 0, 0
	while x & 1 == 0:
		zeros += 1
		x >>= 1

	while x & 1 == 1:
		ones += 1	
		x >>= 1

	power2 = 1 << (zeros + ones)
	num |= power2
	num &= ~(power2 - 1)
	num |= (1 << ones - 1) - 1
	return num

def prev_lower_one(num):
	x, zeros, ones = num, 0, 0
	while x & 1 == 1:
		ones += 1	
		x >>= 1

	while x & 1 == 0:
		zeros += 1
		x >>= 1
	power2 = 1 << (zeros + ones + 1)
	mask = power2 - 1
	num &= ~mask
	num |= ((1 << (ones + 1)) - 1) << (zeros - 1)
	return num

def bitstr(num):
	str = ""
	while num:
		str += "01"[num & 1]
		num >>= 1
	return ''.join(reversed(list(str)))

if __name__ == "__main__":
	from random import randrange
	num = randrange(2 ** 31)
	print num, bitstr(num), num_ones(num)
	x =  next_higher_one(num)
	print "brute higher", x, bitstr(x), num_ones(x)
	x =  next_higher_one_opt(num)
	print "optim higher", x, bitstr(x), num_ones(x)

	x =  next_lower_one(num)
	print "brute lower", x, bitstr(x), num_ones(x)
	x =  prev_lower_one(num)
	print "optim lower", x, bitstr(x), num_ones(x)
