#!/usr/bin/env python
"""
	Given an array of random numbers print all pairs of
	numbers which have a given difference.
"""

import sys
from random import randrange
from collections import defaultdict

try:
	num = int(sys.argv[1])
except:
	print "usage %s [size of array]" % sys.argv[0]
	exit(1)

l = [randrange(num * 4) for i in range(num)]
l.sort()
g = defaultdict(set)

def pairs(l):
	for i in range(num - 1):
		for j in range(i+1, num):
			diff = l[i] - l[j]
			g[abs(diff)].add((l[i], l[j]))
pairs(l)
print l
for k in g.keys():
	if len(g[k]) > 1:
		print k, g[k]
