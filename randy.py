#!/usr/bin/env python
"""
	Given an array of random numbers find the k largest
	numbers.
"""

import sys
from random import randrange

try:
	n, k = map(int, sys.argv[1:])
except:
	print "usage %s [size of array] [how many largest]" % sys.argv[0]
	exit(1)

l = [randrange(n * 10) for i in range(n)]
