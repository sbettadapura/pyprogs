#!/usr/bin/pythyon
import csv
from collections import namedtuple
stock = namedtuple("stock", 'Symbol, Security, Quantity, Share_Price, Total_Purchase_Price, Date_bought, Account_number, Status, Date_sold, Value_per_share, Total_Proceeds, Reportable_gain_loss, Disallowed_Wash_sale, Covered, Original_cost, Original_Acquistion_date')

def func(x, y):
	print "x = ", x, "y = ", y

stocks =  map(stock._make, csv.reader(open('trades_2014.csv', "rb")))
stocks.pop(0)
#gains = reduce(lambda x,y: x.Reportable_gain_loss + y.Reportable_gain_loss, filter(lambda x: x if x.Reportable_gain_loss > 0 else 0, stocks))
#gains = reduce(func, filter(lambda x: x if x.Reportable_gain_loss > 0 else 0, stocks))
gains = filter(lambda x: x.Reportable_gain_loss if x.Reportable_gain_loss > 0 else 0, stocks)
tot_gains = reduce(lambda x, y: x.Reportable_gain_loss+y.Reportable_gain_loss, gains)
print "tot_gains = ", tot_gains
