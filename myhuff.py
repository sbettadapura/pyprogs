#!/usr/bin/env python
"""
	A greedy algorithm to produce Huffman code given a frequency of a set of
	characters. The idea is to keep combining the lowest frequencies into
	a new one, delete the two frquencies and insert the new frequency which
	corresponds to the combination of the two characters. Over time the two
	strings may be not two characters, but rather lists which may contains
	characters or lists containing characters.

	Initially we create a list consisting of a tuple of frquency and a 
	character. As we keep combining frequencies we may run into identical
	frequencies. So to do a tie-break, we add a running counter. So the
	element in the list consists of (frq, running_ctr, character)
	Every time we create a new element with frq1 + frq2, [s1 + s2], we produce
	a new running counter that's the previous plus one. Thus the new element
	is (frq1 + frq2, new_running_counter, [s1 + s2]).

	The next problem is to make sure we insert the new element in the right 
	place without having to do a linear search.
	We use bisect to do this. bisect tells us where to insert the new_key.
	We're done when there's only element left in the list.
"""
from bisect import bisect

def myhuff(freq, seq):
	g = sorted(zip(freq, range(len(freq)), seq), key = lambda x: x[0])
	run_ctr = len(freq)
	while len(g) > 1:
		new_key = reduce(lambda x, y: (x[0] + y[0], run_ctr, [x[2],y[2]]), g[:2])
		g[:2] = []
		idx = bisect(g, new_key)
		g.insert(idx, new_key)
		run_ctr += 1
	return g[0][-1]

def codify(tree, prefix = ""):
	if len(tree) == 1:
		print tree[0], prefix
	else:
		for child, bit in zip(tree, "01"):
			codify(child, prefix + bit)	
if __name__ == "__main__":
	codify(myhuff([4, 5, 6, 9, 11, 12, 15, 16, 20], "abcdefghi"))
