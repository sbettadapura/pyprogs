#!/usr/bin/env python2
import csv
import subprocess
import os
import sys
import time

import boto3
from create_relay import Server, RegionBuilder, Builder

def establish_enclave_relay(server, binary, public_key, client_ip_addr, other_relay_ip_addr):
	server.send_command("sudo %s <params> %s %s %s" % (binary, public_key, client_ip_addr, other_relay_ip_addr))

def new_enclave_relay(builder, regions, relay_names, public_keys):
	region_builders = [RegionBuilder(builder.aws_access_key_id, builder.aws_secret_access_key, region, builder.data_dir) for region in regions]

	servers = [region_builder.make_server(relay_name, region) for region_builder, region, relay_name in zip(region_builders, regions, relay_names)]

	return servers
def usage(progname):
	print "%s <file environmental variables>" % progname

rv = {'CREDENTIALS_FILE', 'REGION_NAME1', 'RELAY_NAME1', 'PUBLIC_KEY1', 'CLIENT1_IP_ADDR', 'REGION_NAME2', 'RELAY_NAME2', 'PUBLIC_KEY2', 'CLIENT2_IP_ADDR', 'RELAY_CONFIG_APP'}

def parse_env(envf):
	vdict = dict()
	f = open(envf)
	for line in f.readlines():
		k, v = line.strip().split('=')
		if k not in rv:
			return k, vdict
		vdict[k] = v	
	return rv.difference(vdict), vdict

if __name__ == '__main__':
	if len(sys.argv) != 2:
		usage()
		sys.exit(1)
	res, vd = parse_env(sys.argv[1])
	if res:
		print "%s is(are) required parameter(s) missing or extraneous variable found in %s" % (res, sys.argv[1])
		sys.exit(1)

	print "%s, CREDENTIALS FILE: \"%s\", RELAY_NAME1: \"%s\", 'REGION_NAME1': \"%s\", PUBLIC_KEY1: \"%s\", CLIENT1_IP_ADDR: \"%s\", RELAY_NAME2: \"%s\", REGION2: \"%s\", PUBLIC_KEY2: \"%s\", CLIENT2_IP_ADDR: \"%s\", RELAY_CONFIG_APP: \"%s\"" %(sys.argv[0], vd['CREDENTIALS_FILE'], vd['RELAY_NAME1'], vd['REGION_NAME1'], vd['PUBLIC_KEY1'], vd['CLIENT1_IP_ADDR'], vd['RELAY_NAME2'], vd['REGION_NAME2'], vd['PUBLIC_KEY2'], vd['CLIENT2_IP_ADDR'], vd['RELAY_CONFIG_APP'])

	WORK_DIRECTORY="/tmp"
	builder = Builder(vd['CREDENTIALS_FILE'], WORK_DIRECTORY)

	server1, server2 = new_enclave_relay(builder, (vd['REGION_NAME1'], vd['REGION_NAME2']), (vd['RELAY_NAME1'], vd['RELAY_NAME2']), (vd['PUBLIC_KEY1'], vd['PUBLIC_KEY2']))

	establish_enclave_relay(server1, vd['RELAY_CONFIG_APP'], vd['PUBLIC_KEY1'], vd['CLIENT1_IP_ADDR'], server2.ip_address)

	establish_enclave_relay(server2, vd['RELAY_CONFIG_APP'], vd['PUBLIC_KEY2'], vd['CLIENT2_IP_ADDR'], server1.ip_address)
