#!/usr/bin/env python
import sys
from collections import defaultdict
def gene_health(gstr, gdict, glenmax):
	gscore = i = 0
	while i < len(gstr):
		ch = gstr[i]
		for j in range(glenmax[ch], -1, -1):
			chr = gstr[i:i+j]
			if chr not in gdict:
				continue
			gscore += gdict[chr]
			if len(chr) == 1:
				i += 1
			elif i < len(gstr) - 1:
				if gstr[i+1] != chr[-1]:
					i += len(chr)
				else:
					i += 1
			else:
				i += 1
			break
		else:
			i += 1

	return gscore

def preproc(genes, health):
	gdict = defaultdict(int)
	glenmax = defaultdict(int)
	for i, x in enumerate(genes):
		gdict[x] += health[i]
		if len(x) == 1:
			glenmax[x] = 1
		else:
			glenmax[x[0]] = len(x)
	return gdict, glenmax

if __name__ == '__main__':
	"""
	n = int(raw_input())
	genes = raw_input().rstrip().split()
	health = map(int, raw_input().rstrip().split())
	s = int(raw_input())
	"""
	f = open(sys.argv[1])
	n = int(f.readline().rstrip())
	genes = f.readline().rstrip().split()
	health = map(int, f.readline().rstrip().split())
	s = int(f.readline().strip())

	gscores = []
	for s_itr in xrange(s):
		#firstLastd = raw_input().split()
		firstLastd = f.readline().rstrip().split()
		first = int(firstLastd[0])
		last = int(firstLastd[1])
		d = firstLastd[2]

		gdict, glenmax = preproc(genes[first:last+1], health[first:last+1])
		gscore = gene_health(d, gdict, glenmax) 
		gscores.append(gscore)

	gscores.sort()
	print gscores[0], gscores[-1]
