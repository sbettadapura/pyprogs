#!/usr/bin/env python

def hanoisort(origin, buffer = [], dest = []):
	if not origin:
		return dest
	low = origin.pop()
	while origin:
		x = origin.pop()
		if x < low:
			buffer.append(low)
			low = x	
		else:
			buffer.append(x)
	dest.append(low)
	hanoisort(buffer, origin, dest)
	return dest
if __name__ == "__main__":
	from random import randrange
	origin = [randrange(1000) for i in range(randrange(25))]
	print origin
	print hanoisort(origin)

