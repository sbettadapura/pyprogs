#!/usr/bin/env python
from random import choice
from random import randrange

def partition(a, k, v):
	left, right, mid = [], [], []
	n = len(a)
	for i in range(n):
		x = a[i]
		if x < v:
			left.append(x)
		elif x > v:
			right.append(x)
		else:
			mid.append(x)
	return (left, mid, right)

def kpart(a, k):
	n = len(a)
	v = choice(a)
	left, mid, right = partition(a, k, v)
	if left == [] and right == []:
		return mid[0]
	elif len(right) == n - k:
		return max(left + mid)
	elif len(right) > n - k:
		return kpart(right, k - len(left) - len(mid))
	else:
		return kpart(left + mid, k)

if __name__ == "__main__":
	import sys
	n = int(sys.argv[1])
	k = int(sys.argv[2])
	a = [randrange(n * n) for i in range(n)]	
	#a = [46, 2, 46, 53, 43, 15, 54, 4, 73]
	print "a = ", a
	print "sorted(a) = ", sorted(a)
	print "kth in a = ", kpart(a, k)	
