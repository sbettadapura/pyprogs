#!/usr/bin/env python
from operator import add
def parens(n):
	if n == 1:
		return ['()']
	x = parens(n - 1)
	return reduce(add, [['(' + l + ')', l + '()', '()' + l] for l in x])[:-1]

if __name__ == "__main__":
	import sys
	n =  int(sys.argv[1])
	for i in  parens(n):
		print i
