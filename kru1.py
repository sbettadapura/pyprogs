from collections import defaultdict
if __name__ == '__main__':
	import sys
	f = open(sys.argv[1])
	g_nodes, g_edges = map(int, f.readline().split())
	g_from = [0] * g_edges
	g_to = [0] * g_edges
	g_weight = [0] * g_edges

	for i in xrange(g_edges):
		g_from[i], g_to[i], g_weight[i] = map(int, f.readline().split())

	gd, es = dict(), set()
	nei = defaultdict(set)
	for x in range(1, g_nodes + 1):
		nei[x].add(x)

	for i in range(len(g_weight)):
		gd[g_weight[i]] = (g_from[i], g_to[i])
	tree_weight = 0
	while len(es) != g_nodes - 1:
		for k in sorted(gd.keys()):
			u, v = gd[k]
			if nei[u] & nei[v] == set():
				es.add((u, v))
				for x in nei[u]:
					if x == u: continue
					nei[x] |= nei[v]
				nei[u] |= nei[v]
				for x in nei[v]:
					if x == v: continue
					nei[x] |= nei[u]
				nei[v] |= nei[u]
				nei[u].add(v)
				nei[v].add(u)
				tree_weight += k
	print g_nodes, len(es)
	print tree_weight
