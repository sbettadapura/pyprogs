#!/usr/bin/env python
"""
	Print all combinations of r characters from a string of n characters, i.e.
	nCr.
"""
def comb(str, cnt, prefix=""):
	if cnt == 0:
		return [prefix]
	if str == "":
		return []
	else:	
		return reduce(lambda x, y: x +y, [comb(str[i+1:], cnt - 1, prefix + str[i]) for i in range(len(str))])
	
if __name__ == "__main__":
	import sys
	try:
		print comb(sys.argv[1], int(sys.argv[2]))
	except:
		print "usage %s [string] [number]" % sys.argv[0]
		exit(1)
