#!/usr/bin/env python
from collections import defaultdict
vertex_pairs = []
import sys
f = open(sys.argv[1])

def solution():
	#vertexes_num, edges_num = map(int, input().split())
	vertexes_num, edges_num = map(int, f.readline().strip().split())
	graph = defaultdict(set)
	vertex_pairs = [(weight, v1, v2) for v1, v2, weight
			#in [map(int, input().split()) for x in range(edges_num)]]
			in [map(int, f.readline().strip().split()) for x in range(edges_num)]]
	vertex_pairs.sort()
	result = 0
	for weight, v1, v2 in vertex_pairs:
		# Made changes in this condition
		if (v2 in graph[v1]) and (v1 in graph[v2]):
			continue
		s = graph.get(v1) or graph.get(v2) or set()
		s.update({v1, v2})
		s.update(graph[v1], graph[v2])
		# Added these lines
		for x in s:
			graph[x] = s
		result += weight
	return result


print(solution())
