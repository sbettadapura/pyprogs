#!/usr/bin/env python
class Tree:
	def __init__(self, val, left = None, right = None):
		self.root = val
		self.left = left
		self.right = right


def lca(tree, p, q):
	cnt = 0
	if tree.root == p or tree.root == q:
		cnt += 1
	for tr in (tree.left, tree.right):
		if not tr:
			continue
		x, ca = lca(tr, p, q)
		if x == 2:
			return (2, ca)
		cnt += x	
		if cnt == 2:
			return (2, tree)
	return (cnt, None)

if __name__ == "__main__":
	t1 = Tree(80, Tree(90), Tree(100))
	t2 = Tree(20, Tree(70), t1)
	t3 = Tree(50, Tree(40), Tree(60))
	t4 = Tree(10, t3, t2)
	cnt, tx = lca(t4, 60, 70)
	print tx.root
