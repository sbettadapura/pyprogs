#!/usr/bin/py

import sys
x = int(sys.argv[1])
save, mask = x, 1
while x:
	mask *= 10
	x /= 10
mask /= 10
x = save
while mask:
	if x / mask != x % 10:
		print save, " is not a palindrome"
		break
	x %= mask
	x /= 10
	mask /= 100
else:
	print save, " is a palindrome"
