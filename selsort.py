#!/usr/bin/env python

"""
	Write an iterative version of selection sort.
"""

from random import randrange
import sys
try:
	n = int(sys.argv[1])
except:
	print "usage: %s [number of elements in array]" % sys.argv[0]
	exit(1)

L = [randrange(n * 10) for x in range(n)]
print "before sorting", L

def selsort(L):
	for i in range(len(L)-1, 0, -1):
		ind = i
		for j in range(i):
			if L[j] > L[ind]:
				ind = j
		L[i], L[ind] = L[ind], L[i]

selsort(L)
print "after sorting", L
