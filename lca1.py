#!/usr/bin/env python
class Tree:
	def __init__(self, val, left = None, right = None):
		self.root = val
		self.left = left
		self.right = right

def exists(tree, node):
	if tree is None:
		return False
	elif tree.root == node:
		return True
	else:
		return exists(tree.left, node) or exists(tree.right, node)

def lca(tree, p, q):
	if tree is None:
		return None
	elif tree.root == p or tree.root == q:
		return tree

	l = exists(tree.left, p)
	r = exists(tree.right, q)
	if l and r or (not l and not r):
		return tree
	elif r:
		return lca(tree.right, p, q)
	else:
		return lca(tree.left, p, q)

if __name__ == "__main__":
	t1 = Tree(80, Tree(90), Tree(100))
	t2 = Tree(20, Tree(70), t1)
	t3 = Tree(50, Tree(40), Tree(60))
	t4 = Tree(10, t3, t2)
	tx = lca(t4, 70, 100)
	print tx.root
