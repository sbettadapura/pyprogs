#!/bin/python

import os
import sys
#
# Complete the buildPalindrome function below.
#
def ispal(arr):
	for i in range(len(arr) / 2):
		if arr[i] != arr[len(arr) - 1 - i]:
			return False
	else:
		return True
	
def maxpal(arr):
	lastc, res, newarr = arr[-1], arr[-1:], arr[:-1]
	while newarr:
		if not lastc in newarr:
			return res
		rind = newarr.rindex(lastc)
		if ispal(arr[rind:]):
			res = arr[rind:]
			if res != len(res) * lastc:
				return res
		newarr = newarr[:rind]
	return res
	
def checkpal(asub, b, cpal):
	if b == '':
		if len(asub) == 1:
			return asub
		else:
			return checkpal(asub[0], asub[1:])
	
	asubl = list(asub)
	arev = ''.join([y for y in reversed(asubl)])
	if not arev in b:
		x = arev[1:]
		while x:
			y = asub + x
			if x in b and ispal(y):
				return y
			x = x[1:]
		return ''
	else:
		ress = []
		while b:
			if not arev in b:
				break
			ind =  b.index(arev)
			if ind > 0:
				mpal = maxpal(b[:ind])
				#print "In checkpal(), b[:ind] = ", b[:ind], "asub = ", asub, "mpal = ", mpal
				if len(cpal) > len(mpal):
					mpal = cpal
				res = asub + mpal + arev
			else:
				res = asub + arev
			ress.append(res)
			b = b[ind+1:]
		if ress == []:
			return ''
		ress.sort(key = lambda x: (-len(x), x))
		return ress[0]

def pickpal(asub, b, res, max_res, cpal, partials, que):
	if len(res) > len(max_res):
		max_res = res
	elif len(res) == len(max_res):
		if res < max_res:
			max_res = res
	#if res != '' and que == DO_QUE:
	if que == DO_QUE:
		partials.append((asub, b, cpal, res))
	return max_res

def reportpal(max_res):
	if max_res == '':
		max_res = str(-1)
	return max_res

def closepal(curr_pal, a, i):
	if curr_pal is None:
		if a[i] == a[i+1]:
			curr_pal = (i, i + 2)
		elif a[i] == a[i+2]:
			curr_pal = (i, i + 3)
	elif curr_pal[1] >= len(a):
		curr_pal = None
	elif a[i] == a[curr_pal[1]]:
		curr_pal = (i, curr_pal[1] + 1)
	else:
		curr_pal = None

	return curr_pal

DO_QUE, NO_QUE = range(1, 3)
def process_partials(a, max_res, partials):
	while partials:
		asub, b, cpal, res = partials.pop()
		if len(asub) < 2:
			continue
		if cpal != '':
			cpalpl = asub[-1] + cpal + asub[-1]
			if not cpalpl in a:
				cpalpl = ''
		else:
			cpalpl = ''
		res = checkpal(asub[:-1], b, cpalpl)
		max_res = pickpal('', '', res, max_res, '', '', NO_QUE)
	return max_res

def buildPalindrome(a, b):
	curr_pal, max_res = None, ''
	i, partials = len(a), []
	while i > 0:
		if i > 0 and i < len(a) - 3:
			curr_pal = closepal(curr_pal, a, i)
		else:
			curr_pal = None
		partials = []
		for j in xrange(i-1, -1, -1):
			if curr_pal:
				cpal = a[curr_pal[0]:curr_pal[1]]
			else:
				cpal = ''
			res = checkpal(a[j:i], b, cpal)
			max_res = pickpal(a[j:i], b, res, max_res, cpal, partials, DO_QUE)
			if res == '' and i - j > 1:
				max_res = process_partials(a, max_res, partials)
				i = j
				break
		else:
			i -= 1
	return reportpal(max_res)

if __name__ == '__main__':
	f = open(sys.argv[1])
	#fptr = open(os.environ['OUTPUT_PATH'], 'w')
	#t = int(raw_input())
	t = int(f.readline().rstrip())
	for t_itr in xrange(t):
		#a = raw_input()
		a = f.readline().rstrip()
		#b = raw_input()
		b = f.readline().rstrip()
		#fptr.write(result + '\n')
		result = buildPalindrome(a, b)
		print result
	#fptr.close()
