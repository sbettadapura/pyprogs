#!/bin/python

import os
import sys
#
# Complete the buildPalindrome function below.
#
def ispal(arr):
	for i in range(len(arr) / 2):
		if arr[i] != arr[len(arr) - 1 - i]:
			return False
	else:
		return True
	
def maxpal(arr):
	for i in range(len(arr)):
		if ispal(arr[i:]):
			return arr[i:]	

def checkpal(asub, b, arev):
	if b == '':
		if len(asub) == 1:
			return asub
		else:
			return checkpal(asub[0], asub[1:])
	
	if not arev in b:
		x = arev[1:]
		while x:
			y = asub + x
			if x in b and ispal(y):
				return y
			x = x[1:]
		return ''
	else:
		ress = []
		while b:
			if not arev in b:
				break
			ind =  b.index(arev)
			if ind > 0:
				res = asub + maxpal(b[:ind]) + arev
			else:
				res = asub + arev
			ress.append(res)
			b = b[ind+1:]
		if ress == []:
			return ''
		ress.sort(key = lambda x: (len(x), x))
		return ress[0]

def buildPalindrome(a, b):
	max_res = ''
	for i in xrange(len(a) - 1, -1, -1):
		revi = ''
		for j in xrange(i+1, len(a) + 1):
			revi = a[j - 1] + revi
			res = checkpal(a[i:j], b, revi)
			if len(res) > len(max_res):
				max_res = res
			elif len(res) == len(max_res):
				if res < max_res:
					max_res = res
	if max_res == '':
		max_res = str(-1)
	return max_res

if __name__ == '__main__':
	f = open(sys.argv[1])
	#fptr = open(os.environ['OUTPUT_PATH'], 'w')
	#t = int(raw_input())
	t = int(f.readline().rstrip())
	for t_itr in xrange(t):
		#a = raw_input()
		a = f.readline().rstrip()
		#b = raw_input()
		b = f.readline().rstrip()
		#fptr.write(result + '\n')
		result = buildPalindrome(a, b)
		print result
	#fptr.close()
