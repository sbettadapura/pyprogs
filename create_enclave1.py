#!/usr/bin/env python2
import csv
import subprocess
import os
import sys
import time

import boto3
from create_relay import Server, RegionBuilder, Builder

def establish_enclave_relay(server, binary, public_key, port_num, other_relay_ip_addr):
	server.send_command("sudo cp %s /usr/bin" % binary)
	server.send_command("sudo chmod +x /usr/bin/%s" % binary)
	server.send_command("sudo mkdir -p /etc/rc_dir/conf")
	server.send_command("sudo mkdir -p /etc/ca/publickeys")
	server.send_command("sudo cp %s /etc/ca/publickeys" % public_key)
	server.send_command("sudo %s %d /etc/rc_dir" % (binary, port_num))

def new_enclave_relay(builder, regions, public_keys):
	region_builders = [RegionBuilder(builder.aws_access_key_id, builder.aws_secret_access_key, region, builder.data_dir) for region in regions]

	relay_names = ['relay_1', 'relay_2']
	servers = [region_builder.make_server(relay_name, region) for region_builder, region, relay_name in zip(region_builders, regions, relay_names)]

	return servers

def usage(progname):
	print "%s <credentials_file> <region1> <public_key_1> <region_2> <public_key_2> <relay_config_app> <port_num>" % progname

rv = ['CREDENTIALS_FILE', 'REGION_NAME1', 'PUBLIC_KEY1', 'REGION_NAME2', 'PUBLIC_KEY2', 'RELAY_CONFIG_APP', 'PORT_NUM']

def parse_env(argv):
	vdict = dict()
	for i, arg in enumerate(argv):
		vdict[rv[i]] = arg	
	return vdict

if __name__ == '__main__':
	if len(sys.argv) != 8:
		usage(sys.argv[0])
		sys.exit(1)
	vd = parse_env(sys.argv[1:])
	for x, y in zip(rv, [vd[x] for x in rv]):
		print x, ":", y, ",",
	print '\n'
	WORK_DIRECTORY="/tmp"
	builder = Builder(vd['CREDENTIALS_FILE'], WORK_DIRECTORY)

	server1, server2 = new_enclave_relay(builder, (vd['REGION_NAME1'], vd['REGION_NAME2']), (vd['PUBLIC_KEY1'], vd['PUBLIC_KEY2']))

	establish_enclave_relay(server1, vd['RELAY_CONFIG_APP'], vd['PUBLIC_KEY1'], int(vd['PORT_NUM']),  server2.ip_address)

	establish_enclave_relay(server2, vd['RELAY_CONFIG_APP'], vd['PUBLIC_KEY2'], int(vd['PORT_NUM']),  server1.ip_address)
