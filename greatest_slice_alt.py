#!/usr/bin/env pyhon
"""
	Given an array find the slice with greatest sum.
	[12, 2, 15, -4, 18] [15, -4], 18] is the answer.
	If all numbers are negative the empty [] is the solution.
"""

#A = [54, 32, -23, 18, -8]
#A = [-2, 7, 1, 5, 19, -2, 21, 42]
#A = [-2, 7, 1, 5, 19, -2, -21, 42]
#

def gr_slice(l):
	n = len(l)
	best = -999999
	for start in range(n):
		psum = l[start]
		best = max(best, psum)
		for succ in range(start+1, n):
			psum += l[succ]
			best = max(best, psum)
	return best

"""
	given a file conataining arrays of numbers separated by while spaces
	on different lines, combine all of them into a list.
"""
def file_to_arr(fd):
	return [int(x) for x in reduce(lambda x, y: x + " " + y,  [x.rstrip() for x in fd.readlines()]).split()]

if __name__ == "__main__":
	import sys
	print gr_slice(file_to_arr(open(sys.argv[1])))
