#!/usr/bin/env python

def bestof(n, r, d = dict()):
	if n < r:
		return 0
	if r == 0:
		return 1
	bsum = 0
	for a in ((n-1, r-1), (n-1, r)):
		tmp = d.get(a, 0)
		if tmp == 0:
			tmp = d[a] = bestof(a[0], a[1], d)
		bsum += tmp
	return bsum
if __name__ == "__main__":
	import sys
	try:
		n = int(sys.argv[1])
	except:
		print "usage %s <odd number>" % sys.argv[0]
	print bestof(n, n/2 + 1)
