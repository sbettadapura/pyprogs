#!/usr/bin/env python

def bestof(n, r):
	if n < r:
		return 0
	if r == 0:
		return 1
	return bestof(n-1, r) + bestof(n-1, r - 1)

if __name__ == "__main__":
	import sys
	try:
		n = int(sys.argv[1])
	except:
		print "usage %s <odd number>" % sys.argv[0]
	print bestof(n, n/2 + 1)
