#!/usr/bin/env python
def pangrams(s):
    from collections import defaultdict
    freq = defaultdict(int)
    for x in s:
        freq[x.lower()] += 1
    if all([freq[x] >= 1 for x in "abcdefghijklmnopqrstuvwxyz"]):
        return "pangram"
    else:
        return "not pangram"

print pangrams("We promptly judged antique ivory buckles for the next prize")
