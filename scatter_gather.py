#!/usr/bin/env python

"""
	Given a list of tuples (starting address, length) coalesce
	when possible. And compact the new list so holes move to the end.
"""
sclist = [(20, 40), (70, 30), (100, 60), (160, 50), (210, 20), (240, 80)]

print sclist
run_ad, run_len = sclist[0]
rp,wp = 1, 0
while rp < len(sclist):
	curr_ad, curr_len = sclist[rp]
	if run_ad + run_len == curr_ad:
		run_len += curr_len
	else:
		sclist[wp] = (run_ad, run_len)
		wp += 1
		run_ad, run_len = sclist[rp]
	rp += 1
sclist[wp] = (run_ad, run_len)
for i in range(wp+1, rp):
	sclist[i] = (0, 0)
print sclist
