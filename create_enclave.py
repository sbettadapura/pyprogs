#!/usr/bin/env python2
import csv
import subprocess
import os
import sys
import time

import boto3
from create_relay import Server, RegionBuilder, Builder

def establish_enclave_relay(server, binary, public_key, port_num, other_relay_ip_addr):
	server.send_command("sudo cp %s /usr/bin" % binary)
	server.send_command("sudo chmod +x /usr/bin/%s" % binary)
	server.send_command("sudo mkdir -p /etc/rc_dir/conf")
	server.send_command("sudo mkdir -p /etc/ca/publickeys")
	server.send_command("sudo cp %s /etc/ca/publickeys" % public_key)
	server.send_command("sudo %s %d /etc/rc_dir" % (binary, port_num))

def new_enclave_relay(builder, regions):
	region_builders = [RegionBuilder(builder.aws_access_key_id, builder.aws_secret_access_key, region, builder.data_dir) for region in regions]

	relay_names = ['relay_1', 'relay_2']
	servers = [region_builder.make_server(relay_name, region) for region_builder, region, relay_name in zip(region_builders, regions, relay_names)]

	return servers

def usage(progname, rv):
	print "Usage: ", progname,
	for x in rv:
		print x,
	print '\n'

rv = ['CREDENTIALS_FILE', 'REGION_NAME1', 'PUBLIC_KEY1', 'REGION_NAME2', 'PUBLIC_KEY2', 'RELAY_CONFIG_APP', 'PORT_NUM']

if __name__ == '__main__':
	if len(sys.argv) != 1 + len(rv):
		usage(sys.argv[0], rv)
		sys.exit(1)
	vd = dict(zip(rv, sys.argv[1:]))
	for x in rv:
		print x, ":", vd[x], ",",
	print '\n'
	WORK_DIRECTORY="/tmp"
	builder = Builder(vd['CREDENTIALS_FILE'], WORK_DIRECTORY)

	server1, server2 = new_enclave_relay(builder, (vd['REGION_NAME1'], vd['REGION_NAME2']))

	establish_enclave_relay(server1, vd['RELAY_CONFIG_APP'], vd['PUBLIC_KEY1'], int(vd['PORT_NUM']),  server2.ip_address)

	establish_enclave_relay(server2, vd['RELAY_CONFIG_APP'], vd['PUBLIC_KEY2'], int(vd['PORT_NUM']),  server1.ip_address)
