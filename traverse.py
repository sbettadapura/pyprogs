#!/usr/bin/env python
"""
	Hetland's algorithm to 'walk' a graph
"""

def walk (g, s):
	P, Q = dict(), set()
	Q.add(s)
	P[s] = None
	while Q:
		u = Q.pop()
		for v in g[u].difference(P):
			Q.add(v)
			P[v] = u
	return P

if __name__ == '__main__':
	g = [{1, 2, 3},
	 	{0, 2, 4},
	 	{0, 3},
     	{0, 1, 2},
	 	{1}]
	path_g = walk(g, 0)
	print path_g

	h = [ {1, 2},
		  {0, 3},
		  {0, 4},
		  {1, 5},
		  {2, 5},
          {3, 4}]
	path_h = walk(h, 0)
	print path_h
