#!/usr/bin/env python
"""
	A riddle from 1612 in France. What's the minimum number of weights that
  	you can use on either side of a balance (but only one of each kind) and 
	produce weights from 1 to 40 ? The answer is 1, 3, 9, 27. 2 = 3-1, 4 = 3+1,
	5 = 9 - 3 - 1 etc. It also works for higher powers of 3 like 
	1, 3, 9, 27, 81, ...

	The simpler problem is: given weights 3 ** 0, 3 ** 1 ... 3 ** n, can you 
 	figure out all the weights you can derive and describe the derivation 
	for each weight ?
"""
	
import sys
try:
	num = int(sys.argv[1])
except:
	print "usage: %s [n] to use weights 3**0, 3**1 ... 3**n" % sys.argv[0]
	exit(1)

l = [[1]]
for i in range(1, num+1):
	j = 3 ** i
	l += [[j]] + reduce(lambda x, y: x + y, [[y + [j] for y in [[q * i for q in x] for x in [x[:] for x in l]]] for i in (1, -1)])

l.sort(key = sum)
for i in l:
	print sum(i), ":", i
