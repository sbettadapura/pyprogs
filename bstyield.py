#!/usr/bin/env python
class Tree:
	def __init__(self, val, left = None, right = None):
		self.root = val
		self.left = left
		self.right = right
	def in_order(self):
		if self.left:
			for x in self.left.in_order():
				yield x
		yield self.root
		if self.right:
			for x in self.right.in_order():
				yield x
if __name__ == "__main__":
	t1 = Tree(5)
	t2 = Tree(20)
	t3 = Tree(10, t1, t2)
	t4 = Tree(40)
	t5 = Tree(60)
	t6 = Tree(50, t4, t5)
	t7 = Tree(30, t3, t6)
	for x in t7.in_order():
		print x
