#!/usr/bin/env python

X_AXIS, Y_AXIS = range(2)
def rot_ac(L, src, tgt, size):
	save_tgt = []

	tgt_st = tgt[0]
	tgt_st_x = tgt_st[0]
	tgt_st_y = tgt_st[1]
	tgt_axis = tgt[1]
	tgt_incr = tgt[2]

	for i in range(size - 1):
		if tgt_axis == Y_AXIS:
			save_tgt.append(L[tgt_st_x][tgt_st_y + tgt_incr * i])
			L[tgt_st_x][tgt_st_y + tgt_incr * i] = src[i]
		else:
			save_tgt.append(L[tgt_st_x + tgt_incr * i][tgt_st_y])
			L[tgt_st_x + tgt_incr * i][tgt_st_y] = src[i]
	return save_tgt

def rotate(L):
	n = len(L)
	for r in range(n):
		size = n - 2 * r
		if size < 2:
			break
		br = ((r, r), Y_AXIS, 1)
		rc = ((r, n -  r - 1), X_AXIS, 1)
		tr = ((n - r - 1, n - r - 1), Y_AXIS, -1)
		lc = ((n - r - 1, r), X_AXIS, -1)
		src = []
		for i in range(size - 1):
			src.append(L[r][r + i])
		for tgt in (rc, tr, lc, br):
			src = rot_ac(L, src, tgt, size)
if __name__ == "__main__":
	import sys
	from random import randrange
	try:
		num = int(sys.argv[1])
	except:
		print "usage: %s [size of matrix to rotate]" % sys.argv[0]
		exit(1)
	L = [[0] * num for i in range(num)]
	for i in range(num):
		for j in range(num):
			L[i][j] = randrange(num * 10)
	for i in range(num):
		print L[num - i - 1]
	rotate(L)
	print "after rotation"
	for i in range(num):
		print L[num - i - 1]
