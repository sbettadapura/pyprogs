#!/usr/bin/env python

class Linked:
	def __init__(self, val, next = None):
		self.val = val
		self.next = next

def advance(linked, num, steps):
	for i in range(num):
		for j in range(steps):
			if linked is not None:
				linked = linked.next
			else:
				return None
	return linked
	
def prnt(ll):
	while ll is not None:
		print ll.val,
		ll = ll.next
	print "\n",

if __name__ == "__main__":
	a = [3, 5, 7, 11, 13, 17, 19, 23]
	head = None
	for x in reversed(a):
		tmp = Linked(x)
		if head is None:
			head = tmp
		else:
			tmp.next = head
			head = tmp
	prnt(head)

	import sys
	beg, end = map(int, sys.argv[1:])
	lbeg = advance(head, beg, 1)
	lend = advance(head, end, 1)
	print lbeg.val, lend.val
	lend.next = lbeg

	i = 1
	lslow = lfast = head
	while 1:
		if lslow is None or lfast is None:
			break
		lslow = advance(head, i, 1)
		lfast = advance(head, i, 2)
		print lslow.val, lfast.val
		if lslow.val == lfast.val:
			break
		i += 1	
	print lslow.val, lfast.val
	i = 1
	lslow = lslow.next
	while lslow.val != lfast.val:
		lslow = lslow.next
		i += 1
	print i
	lslow = head
	lfast = advance(head, i, 1)
	while lslow.val != lfast.val:
		lslow = lslow.next
		lfast = lfast.next
	print lslow.val	
