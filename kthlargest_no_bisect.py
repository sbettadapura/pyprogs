#!/usr/bin/env python
"""
	Given an array of random numbers find the k largest
	numbers.
"""
import sys
from datetime import datetime
from random import randrange

try:
	n, k = map(int, sys.argv[1:])
except:
	print "usage %s [size of array] [how many largest]" % sys.argv[0]
	exit(1)

l = [randrange(n * 5) for i in range(n)]
"""
lk = sorted(l[:k])

for lval in l[k:]:
	for j in range(k - 1, -1, -1):
		if lval > lk[j]:
			lk.insert(j+1, lval)
			lk.pop(0)
			break	
print k, " largest elements computed = ", lk
"""
print "starting time: ", datetime.now().isoformat()
lk = [-x for x in sorted(l[:k], reverse = True)]

for lval in l[k:]:
	for j in range(k - 1, -1, -1):
		if -lval < lk[j]:
			lk.insert(j+1, -lval)
			lk.pop()
			break	
print k, " largest elements computed = ", sorted([-x for x in lk])
print "ending time: ", datetime.now().isoformat()
