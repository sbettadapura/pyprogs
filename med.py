#!/usr/bin/env python
from random import choice
from random import randrange

def partition_noes(a, v):
	small, large = 0, len(a)
	while small < large:
		if a[small] < v:
			small += 1
		else:
			large -= 1
			a[small], a[large] = a[large], a[small]
	return a[:small], a[large:]
	
def partition(a, v):
	left, right = [], []
	n = len(a)
	for i in range(n):
		x = a[i]
		if x < v:
			left.append(x)
		else:
			right.append(x)
	return (left, right)

def kpart(a, k):
	n = len(a)
	v = choice(a)
	left, right = partition_noes(a, v)
	if len(right) == n - k:
		return left, right
	elif len(right) > n - k:
		l, r =  kpart(right, k - len(left))
		return left + l, r
	else:
		l, r =  kpart(left, k)
		return l, r + right

if __name__ == "__main__":
	import sys
	n = int(sys.argv[1])
	a = [randrange(n * n) for i in range(n)]	
	print "a = ", a
	print "sorted(a) = ", sorted(a)
	l, r = kpart(a, (n + 1) / 2)	
	print l, r
	if n & 1:
		print "median of a = ", max(l)
	else:
		print "median of a = ", (max(l) + min(r)) * 1.0 / 2
