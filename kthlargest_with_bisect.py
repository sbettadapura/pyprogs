#!/usr/bin/env python
"""
	Given an array of random numbers find the k largest
	numbers.
"""

def k_bisect(l):
	lk = sorted(l[:k])
	for i, lval in enumerate(l[k:]):
		idx = bisect(lk, lval)
		lk.insert(idx, lval)
		lk.pop(0)
	print k, " largest elements computed = ", lk

def k_bisect_invert(l):
	lk = [-x for x in sorted(l[:k], reverse=True)]
	for lval in l[k:]:
		idx = bisect(lk, -lval)
		lk.insert(idx, -lval)
		lk.pop()
	print k, " largest elements computed = ", sorted([-x for x in lk])

def k_heap(l):
	from heapq import heapify, heappush, heappop
	lk = l[:k]
	heapify(l)
	for i, lval in enumerate(l[k:]):
		heappush(lk, lval)
		heappop(lk)
	print k, " largest elements computed = ", lk

def k_sort(l):
	lk = [-x for x in sorted(l[:k], reverse=True)]
	for i, lval in enumerate(l[k:]):
		lk.append(-lval)
		lk.sort()
		lk.pop()
	print k, " largest elements computed = ", sorted([-x for x in lk])

if __name__ == "__main__":
	algos = {"heap": k_heap, "sort": k_sort, "bisect": k_bisect, "invert": k_bisect_invert}

	import sys
	from random import randrange
	from bisect import bisect
	from datetime import datetime
	try:
		n, k = map(int, sys.argv[1:3])
	except:
		print "usage %s [size of array] [how many largest][algorithm]" % sys.argv[0]
		exit(1)
	prog = sys.argv[3]
	if not prog in algos:
		print "no algorithm corresponding to ", prog, " found"
		exit(1)

	l = [randrange(n * 10) for i in range(n)]
	print "starting: ", datetime.now().time().isoformat()
	algos[prog](l)
	print "ending: ", datetime.now().time().isoformat()
