#!/usr/bin/env python
def pickpair(l, psum):
	n = len(l)
	i, j = 0, n -1
	while i < j:
		tsum = l[i] + l[j]
		if tsum == psum:
			return l[i], l[j]
		if tsum > psum:
			j -= 1
		else:
			i += 1
	return None	

import sys
from random import randrange
try:
	num = int(sys.argv[1])
except:
	print "usage %s [size of random array]" % sys.argv[0]
	exit(1)

l = [randrange(num * num) for i in range(num)]
a = l[randrange(num)]
b = l[randrange(num)]
while a == b:
	b = l[randrange(num)]
l.sort()
print l 
print a, b
print pickpair(l, a + b)
