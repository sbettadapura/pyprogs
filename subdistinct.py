#!/usr/bin/env python

from collections import defaultdict
def sub_distinct(a):
	n = len(a)
	max_seq, curr_seq = [], []
	freq = defaultdict(int)
	for i in a:
		freq[i]	+= 1
		if freq[i] == 1:
			curr_seq.append(i)
		else:
			if len(max_seq) < len(curr_seq):
				max_seq = curr_seq[:]
			idx = curr_seq.index(i)
			curr_seq = curr_seq[idx+1:]	
			curr_seq.append(i)
			freq = defaultdict(int)
			for x in curr_seq:
				freq[x] = 1
	if len(max_seq) < len(curr_seq):
		max_seq = curr_seq[:]

	return max_seq

def sub_distinct_1(a):
	n = len(a)
	max_seq, curr_seq = [], []
	freq = defaultdict(int)
	for i in a:
		freq[i]	+= 1
		if freq[i] == 1:
			curr_seq.append(i)
		else:
			max_seq = max(max_seq, curr_seq, key = len)[:]
			idx = curr_seq.index(i)
			for j in range(idx):
				freq[curr_seq[j]] = 0
			curr_seq = curr_seq[idx+1:]	+ [i]
	max_seq = max(max_seq, curr_seq, key = len)[:]
	return max_seq

def sub_distinct_2(a):
	max_len = start = 0
	freq = dict()
	for off, i in enumerate(a):
		if not i in freq:
			freq[i]	= off
		else:
			#print i, off, freq[i]
			if freq[i] >= start:
				max_len = max(max_len, off - start)
				start = freq[i] + 1
			freq[i] = off
	max_len = max(max_len, off - start)
	return max_len

def sub_distinct_3(a):
	max_len = start = 0
	freq = dict()
	for off, i in enumerate(a):
		if i in freq and freq[i] >= start:
			max_len = max(max_len, off - start)
			start = freq[i] + 1
		freq[i]	= off
	max_len = max(max_len, off - start)
	return max_len

if __name__ == "__main__":
	from random import randrange
	import sys
	n = int(sys.argv[1])
	l = [randrange(n) for i in range(n + n / 2)]
	print l
	dist = sub_distinct_1(l)
	print dist, len(dist)
	dist = sub_distinct_3(l)
	print dist
