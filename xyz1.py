#!/usr/bin/env python

def hackerrankInString(s):
    # Complete this function
    refstr = "hackerrank"
    i = 0
    for c in refstr:
        if not c in s[i:]:
			return False
		j = s[i:].index(c)
		#print c, s[i:]
        i += j
    return True

"""
print hackerrankInString("knarrekcah")
print hackerrankInString("hackerrank")
print hackerrankInString("hackeronek")
print hackerrankInString("abcdefghijklmnopqrstuvwxyz")
print hackerrankInString("rhackerank")
print hackerrankInString("ahankercka")
print hackerrankInString("hacakaeararanaka")
print hackerrankInString("hhhhaaaaackkkkerrrrrrrrank")
print hackerrankInString("crackerhackerknar")
print hackerrankInString("hhhackkerbanker")
"""
print hackerrankInString("rhackerank")
