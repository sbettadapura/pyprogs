#!/usr/bin/env python

def partition(l):

	pivot = random.choice(l)
	high = len(l)
	piv_low, piv_high = 0, 0
	while piv_high < high:
		if l[piv_high] == pivot:
			piv_high = piv_high + 1
		elif l[piv_high] > pivot:
			high = high - 1
			l[piv_high], l[high] = l[high], l[piv_high]	
		else:
			l[piv_low], l[piv_high] = l[piv_high], l[piv_low]
			piv_high = piv_high + 1
			piv_low = piv_low + 1
	return pivot, piv_low, piv_high

if __name__ == "__main__":
	import random
	import sys
	n = int(sys.argv[1])
	l = [random.randrange(n * n) for i in range(n)]
	print "l = ", l
	print "lsorted = ", sorted(l)
	pivot, low, high = partition(l)
	print "pivot = ", pivot, "low = ", l[:low], "high = ", l[high:]
