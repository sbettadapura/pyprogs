#!/usr/bin/env python

def movepeg(bottom, top, source, buffer, target):
	if top < bottom:
		return

	movepeg(bottom + 1, top, source, target, buffer)
	print "moving Ring#%d from Peg %d to Peg %d" % (pegs[source][-1], source, target)
	pegs[target].append(pegs[source].pop())
	movepeg(bottom + 1, top, buffer, source, target)

if __name__ == "__main__":
	pegs = [[] for i in range(3)]
	import sys
	nrings = int(sys.argv[1])
	pegs[0] = list(range(nrings))
	for i in range(3):
		print pegs[i]
	movepeg(0, nrings - 1, 0, 1, 2)
	for i in range(3):
		print pegs[i]
