#!/usr/bin/env python
"""
	Given a cash register with quarters, dimes, nickels and pennies
	write a program to produce chnage like a typical cashier does.
"""
curr = [25, 10, 5, 1]
	
def make_change(sum, curr):
	cd = dict((size, 0) for size in curr)
	sci = 0
	while sum:
		if cd[curr[sci]] == 0:
			cd[curr[sci]] = sum/curr[sci]
			sum -= cd[curr[sci]] * curr[sci]
			sci += 1
	return cd

def make_change_2(sum, curr):
	ind = 0
	arr = []
	while sum:
		if sum >= curr[ind]:
			sum -= curr[ind]
			arr.append(curr[ind])
		else:
			ind += 1
	return arr

def make_change_3(sum, curr):
	ind = 0
	cd = {}
	while sum:
		if sum >= curr[ind]:
			cd[curr[ind]] = sum / curr[ind]
			sum %= curr[ind]
		ind += 1
	return cd

if __name__ == "__main__":
	import sys
	from random import randrange
	try:
		num = int(sys.argv[1])
	except:
		print "usage %s [number of times you want random amounts changed" % sys.argv[0]
		exit(1)

	for i in range(num):
		sum = randrange(100)
		print sum, make_change(sum, curr)
		print sum, make_change_2(sum, curr)
		print sum, make_change_3(sum, curr)

