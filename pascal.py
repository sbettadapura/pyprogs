#!/usr/bin/env python

def pascal_1(n):
	rows = [[1]]
	for i in range(1, n):
		row = rows[i - 1]
		newr = []
		for j in range(len(row)):
			if j == 0 or j == len(row) - 1:
				newr.append(1)
			else:
				newr.append(row[j] + row[j + 1])	
		rows.append(newr)	
	return rows[-1]
def pascal(n):
	rows = [[1]]
	for i in range(1, n):
		row = rows[i - 1]
		newr = [1]
		for j in range(len(row) - 1):
			newr.append(row[j] + row[j + 1])	
		newr.append(1)
		rows.append(newr)	
	return rows[-1]
if __name__ == "__main__":
	import sys
	print pascal(int(sys.argv[1]))	
	print pascal_1(int(sys.argv[1]))	
