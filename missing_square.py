#!/usr/bin/env python
"""
	Given N X N board, with one forbidden square at (x, y)
	can you fill the board with L-shaped tiles below ?
		----
		-  -
		-  -
		-  -----
		-	   -
		--------
	Assume N is a power of two.
"""	
import sys
try:
	int_args = map(int, sys.argv[1:])
except:
	print "usage %s [size of square] [x-cord of hole] [y-coord] of hole" % sys.argv[0]
	exit(1)

SIZE, pt_x, pt_y = int_args
board = [[0] * SIZE for i in range(SIZE)]
board[pt_x][pt_y] = -1


def board_print():
	for i in range(SIZE):
		print "----" * SIZE
		print (("|%2i " * SIZE + "|") % tuple(board[SIZE - 1 - i]))

	print "----" * SIZE


def opp_pt_in(x, size):
	return size/2-1 if x >= size/2 else size/2


def mid_pt_in(x, size):
	return size/2 if x >= size/2 else size/2 - 1


def opp_pt(x, size):
	return x/size * size + opp_pt_in(x % size, size)


def mid_pt(x, size):
	return x/size * size + mid_pt_in(x % size, size)


def rib(x, y, size):
	mid_x = mid_pt(x, size)
	mid_y = mid_pt(y, size)
	opp_x = opp_pt(x, size)
	opp_y = opp_pt(y, size)
	return ((mid_x, opp_y), (opp_x, opp_y), (opp_x, mid_y))


def immediate_neighbors(x, y):
	if x % 2 == 0:
		if y % 2 == 0:
			return ((x, y+1), (x+1, y+1), (x+1, y))
		else:
			return ((x+1, y), (x+1, y-1), (x, y-1))
	else:
		if y % 2 == 0:
			return ((x-1, y), (x-1, y+1), (x, y+1))
		else:
			return ((x, y-1), (x-1, y-1), (x-1, y))


def label_tile(x, y, lab):
		#print "In label_tile() x = ", x, " y = ", y, " lab = ", lab
		#print "Board before labelling"
		#board_print()
		nlist = immediate_neighbors(x, y)
		#print "nlist = ", nlist
		for (nx, ny) in nlist:
			assert(board[nx][ny] == 0)
			board[nx][ny] = lab
		#print "Board after labelling"
		#board_print()
		return lab + 1


def fill_rib(mids, lab):
	for (x, y) in mids:
		board[x][y] = lab
	#print "After filling mids"
	#board_print()
	return lab + 1


def fill_board(x, y, size, lab=1):
	global board
	#print "In fill_board() x = ", x, " y = ", y, "size = ", size, " lab =", lab
	if size == 2:
		lab = label_tile(x, y, lab)
		return lab

	rib_pts = rib(x, y, size)
	#print "size = ", size, " rib() returned ", mids
	for (p, q) in rib_pts:
		#print "rib() returned p = ", p, " q = ", q
		lab = fill_board(p, q, size/2, lab)
	lab = fill_board(x, y, size/2, lab)
	lab = fill_rib(rib_pts, lab)
	return lab


fill_board(pt_x, pt_y, SIZE, 1)
board_print()
#rib_pts = rib(pt_x, pt_y, sub_size)
#print rib_pts
